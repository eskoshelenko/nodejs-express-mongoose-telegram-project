const { Settings } = require('./lib/manipulation')
const settings = new Settings()
const defaultObj = {
  maxRows: 6,
  alertDelay: 3,
  rateDelay: 1,
  deliveryDelay: 5
}

settings.create(defaultObj)
  .then( _ => {
    console.log('Default settings are applyed')
    process.exit(0)
  })