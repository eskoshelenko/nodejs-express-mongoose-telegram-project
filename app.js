const express = require('express')
const mongoose = require('mongoose')
// const dotenv = require('dotenv')
const app = express()
// import Routes
const userRoute = require('./routes/user')
const driverRoute = require('./routes/driver')
const requestRoute = require('./routes/request')
const messageRoute = require('./routes/message')
const settingsRoute = require('./routes/settings')

// dotenv.config()

//Connect to DB
mongoose.connect(`<MongooseUrl>`, 
  {useNewUrlParser:true, useUnifiedTopology: true, useCreateIndex: true }, 
  () => console.log("Connected to DB!")
)

//Middleware
app.use(express.json())

app.get('/isup', async (req,res) => {
  res.setHeader('content-Type', 'application/json')
  res.send(JSON.stringify(true))
})

// Route Middlewares
app.use('/api/user', userRoute)
app.use('/api/driver', driverRoute)
app.use('/api/request', requestRoute)
app.use('/api/message', messageRoute)
app.use('/api/settings', settingsRoute)


app.listen(3000, () => console.log('Server Up and Running...'))
