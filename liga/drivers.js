const drivers = [
  // CompanyId = 1 (Rubezhnoe)
  {
    driver_id: 2,
    call_id: 1,
    name: 'Никифаренко Станислав',
    phone: '380990500871',
    car_model: 'Киа Рио',
    car_color: 'Черный',
    car_number: 'вв5328еа'
  },
  {
    driver_id: 3,
    call_id: 2,
    name: 'Поликарпов Виталий',
    phone: '380668820561',
    car_model: 'Шевроле Авео',
    car_color: 'Черный',
    car_number: 'ВВ 7406 ВН'
  },
  {
    driver_id: 44,
    call_id: 3,
    name: 'Гайдай Ольга Юрьевна',
    phone: '380955329917',
    car_model: 'Чери Элара',
    car_color: 'Черный',
    car_number: 'ВВ 2316 ВЕ'
  },
  {
    driver_id: 56,
    call_id: 4,
    name: 'Юшков Александр',
    phone: '380954336143',
    car_model: 'Опель Вектра',
    car_color: 'тёмно-вишнёвый',
    car_number: 'ВВ 0607 ВС'
  },
  {
    driver_id: 7,
    call_id: 5,
    name: 'Штангеев Алексей',
    phone: '380662334055',
    car_model: 'ВАЗ 1119',
    car_color: 'бежевый',
    car_number: '8668'
  },
  {
    driver_id: 49,
    call_id: 7,
    name: 'Савченко Саша',
    phone: '380502170702',
    car_model: 'ВАЗ 2111',
    car_color: 'Серый',
    car_number: 'ВВ 1640 АТ'
  },
  {
    driver_id: 39,
    call_id: 8,
    name: 'СНикифоренко Ярослав',
    phone: '380995522300',
    car_model: 'Дачиа Логан',
    car_color: 'Синий',
    car_number: 'ВВ 8101 ЕТ'
  },
  {
    driver_id: 33,
    call_id: 9,
    name: 'Махортов Александр Анатольевич',
    phone: '380950900810',
    car_model: 'Deawoo lanos',
    car_color: 'Зеленый',
    car_number: 'ВВ 4988 ЕС'
  },
  {
    driver_id: 54,
    call_id: 10,
    name: 'Кулиш Рома',
    phone: '380506976948',
    car_model: 'ВАЗ 2103',
    car_color: 'Голубой',
    car_number: 'вв2051ан'
  },
  {
    driver_id: 14,
    call_id: 12,
    name: 'Большаков Сергей',
    phone: '380668617649',
    car_model: 'Дачиа Логан',
    car_color: 'красный',
    car_number: 'вв7653вх'
  },
  {
    driver_id: 158,
    call_id: 13,
    name: 'Ларин Олег Алексеевич',
    phone: '380660688408',
    car_model: 'Шкода Октавия',
    car_color: 'Зеленый',
    car_number: 'ВВ7064ЕС'
  },
  {
    driver_id: 15,
    call_id: 15,
    name: 'Белозеров Роман',
    phone: '380665982008',
    car_model: 'Дачиа Логан',
    car_color: 'Белый',
    car_number: 'ВВ 4589 ЕТ'
  },
  {
    driver_id: 97,
    call_id: 18,
    name: 'Кривиженко Женя',
    phone: '380956845331',
    car_model: 'Deawoo lanos',
    car_color: 'Синий',
    car_number: 'вв 5648ар'
  },
  {
    driver_id: 19,
    call_id: 19,
    name: 'Богомолов Алексей',
    phone: '380997657075',
    car_model: 'ВАЗ 21112',
    car_color: 'бежевый',
    car_number: 'ВВ5794ЕА'
  },
  {
    driver_id: 21,
    call_id: 21,
    name: 'Ковалевский Костантин',
    phone: '380954444472',
    car_model: 'Шкода фабиа',
    car_color: 'красный',
    car_number: '9342'
  },
  {
    driver_id: 55,
    call_id: 24,
    name: 'Капустин Руслан Алексеевич',
    phone: '380997513540',
    car_model: 'Шкода фабиа',
    car_color: 'красный',
    car_number: '9342'
  },
  {
    driver_id: 4,
    call_id: 25,
    name: 'Нестеренко Юра',
    phone: '380507339775',
    car_model: 'Джили эска2 комфорт',
    car_color: 'Черный',
    car_number: 'ВВ7902СИ'
  },
  {
    driver_id: 22,
    call_id: 27,
    name: 'Яцук Евгений',
    phone: '380508862161',
    car_model: 'Iran крайслер',
    car_color: 'Синий',
    car_number: '708'
  },
  {
    driver_id: 104,
    call_id: 53,
    name: 'Лопата Игорь',
    phone: '380506159623',
    car_model: 'Киа Cerato',
    car_color: 'Голубой',
    car_number: 'ВВ 4955 ЕО'
  },
  // CompanyId = 3 (Kremennaya)
  {
    driver_id: 113,
    call_id: 101,
    name: 'Сербин Евгений Викторович',
    phone: '380502622333',
    car_model: 'ВАЗ 2106',
    car_color: 'Бирюзовый',
    car_number: 'ВВ 4191 ВК'
  },
  {
    driver_id: 114,
    call_id: 102,
    name: 'Вовк Андрей Григорьевич',
    phone: '380955552857',
    car_model: 'Шкода СуперБ',
    car_color: 'Черный',
    car_number: 'ВВ 7427 ЕВ'
  },
  {
    driver_id: 115,
    call_id: 103,
    name: 'Кульпин Валерий Анатольевич',
    phone: '380502858844',
    car_model: 'ВАЗ 21099',
    car_color: 'Синий',
    car_number: 'ВВ 2839 СМ'
  },
  {
    driver_id: 116,
    call_id: 104,
    name: 'Бойправ Павел Вячеславович',
    phone: '380506191443',
    car_model: 'Deawoo lanos',
    car_color: 'Синий',
    car_number: 'ВВ 7503 ЕА'
  },
  {
    driver_id: 117,
    call_id: 105,
    name: 'Кравцов Сергей Николаевич',
    phone: '380994068130',
    car_model: 'ВАЗ 2110',
    car_color: 'серый',
    car_number: 'ВВ 4921 СХ'
  },
  {
    driver_id: 118,
    call_id: 106,
    name: 'Герасименко Алексей Владимироваич',
    phone: '380507064117',
    car_model: 'ВАЗ 2106',
    car_color: 'оранжевый',
    car_number: 'ВВ 4732 АУ'
  },
  {
    driver_id: 119,
    call_id: 108,
    name: 'Запорожец Дмитрий Владимирович',
    phone: '380950471617',
    car_model: 'ВАЗ 2114',
    car_color: 'Зеленый',
    car_number: 'ВВ 4919 АО'
  },
  {
    driver_id: 120,
    call_id: 109,
    name: 'Кучеренко Денис Александрович',
    phone: '380632612336',
    car_model: 'ВАЗ 2109',
    car_color: 'красный',
    car_number: '56454 АМ'
  },
  {
    driver_id: 122,
    call_id: 112,
    name: 'Фишер Виктор Фризонтович',
    phone: '380505429766',
    car_model: 'ВАЗ 2107',
    car_color: 'Бежевый',
    car_number: '4684'
  },
  {
    driver_id: 124,
    call_id: 117,
    name: 'Грачёв Александр Раильевич',
    phone: '380500266041',
    car_model: 'ВАЗ 2109',
    car_color: 'красный',
    car_number: 'ВВ 0761 ЕC'
  },
  {
    driver_id: 125,
    call_id: 127,
    name: 'Самойлов Александр Анатольевич',
    phone: '380508699888',
    car_model: 'ВАЗ 2107',
    car_color: 'Синий',
    car_number: 'АН 1739 АС'
  },
]

module.exports = drivers