const { Driver } = require('./lib/manipulation')
const drivers = new Driver()
const ligaDrivers = require("./liga/drivers")

ligaDrivers.forEach(driver => drivers.ligaCreate(driver))
