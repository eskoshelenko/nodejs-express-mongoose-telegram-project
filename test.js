const {Liga, Request} = require('./lib/manipulation')
const asterisk = require('./lib/asteriskContact')
const { locations, companies } = require('./liga/locality')
const liga = new Liga(companies, locations)

// const request = { 
//   pickup_address: { 
//     ru: { 
//       town: 'Рубежное', 
//       street: 'Победителей проспект', 
//       building: '25', 
//       comment: '', 
//       lat: 49.02569, 
//       lng: 38.3685 
//     },
//     ua: { 
//       town: 'Рубіжне',
//       street: 'Переможців проспект',
//       building: '25',
//       comment: '', 
//       lat: 49.02569,
//       lng: 38.3685 
//     },
//     en: { 
//       town: 'Rubizhne',
//       street: 'Peremozhtsiv Avenue',
//       building: '25',
//       comment: '',
//       lat: 49.02569,
//       lng: 38.3685 
//     } 
//   },
//   car_extras: [],
//   driver_extras: [],
//   _id: '6062de2b2547a645abe6f466',
//   chat_id: 711718203,
//   name: 'UDjIn',
//   state: 'processed',
//   drivers: [],
//   pickup_time: '2021-03-28T11:22:39',
//   create_time: '2021-03-28T11:15:39',
//   complete_time: '',
//   car_type: '6',
//   comment: 'Request from telegram',
//   destinations: [],
//   __v: 0 
// }

// liga.restoreRequestRPC(136490)
// curl -X POST -H "Content-Type: application/json"  -d '{"jsonrpc":"2.0","method":"order.create","params":{"company_id":1,"type_id":48,"price":40,"phone":"380503555204","car_type_id":6,"tariff_id":15,"pickup_time":"2021-03-18T17:00:00","discount_price":0.0,"discount_percent":0.0,"discount_driver_compensation":0.0,"auto_dispatch":true,"notes":"Тест! Не брать!","comment":"Тест! Не брать!","distance":0,"pickup_address":{"locality_id":2,"street":"улица Красная","building":"12","latlng":{"lat":59.897781,"lng":30.405951}}},"id":123123312}' https://pbx_key:b2e93c15c05ad710bcf39ed6f0a5aaec@api-senat-rubezhn.ligataxi.com/rpc

asterisk.sendFile("0506159623", "0952121169")