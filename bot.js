const express = require('express')
const {Messages} = require('./lib/messages')
const {commands} = require(`./lib/buttons`)
const { User, Request, Message, Driver, Settings, Liga } = require('./lib/manipulation')
const tokenUser = "<UserBotToken>"
const tokenDriver = "<DriverBotToken>"
const { locations, companies } = require('./liga/locality')
const icon = require('./lib/icons')
const TelegramBot = require('node-telegram-bot-api')
const app = express()
const ligaApp = express()

app.get('/isup', async (req,res) => {
  res.setHeader('content-Type', 'application/json')
  res.send(JSON.stringify(true))
})

app.listen(3002, () => console.log('Bot Up and Running...'))

const users = new User(),  drivers = new Driver(), requests = new Request(), messages = new Message(), settings = new Settings(), liga = new Liga(companies, locations)
const logo = `\u24BA\u24B6\u24C8\u24E8\n\u24C9\u24B6\u24E7\u24D8\n`.toUpperCase()
const sender = new Messages('ru', logo)
const timeoutObj = {}
const minute = 60 * 1000, second = 1000
const updateTime = requests.setDate(0, '19:30:00')
const updateDelay = requests.setTimestamp(updateTime) >= Date.now() ? requests.setTimestamp(updateTime) - Date.now()
  : 24 * 60 * minute - Date.now() + requests.setTimestamp(updateTime)
const logoOffset = 50
function updateMessages () {
  users.find().then(usersArr => {
    usersArr.forEach(async user => {
      const currentRequest = await requests.current(user.chat_id)

      if (currentRequest) return
      else messages.findUserMsg(user.chat_id)
        .then( message => {
          if (message) sender.updateUserMessage(userBot, user.chat_id, message)
              .then(res => messages.edit({ ...message, user:  res }))
          else {
            if(user.menu_message_id){
              sender.clearHistory(userBot, user.chat_id, [user.menu_message_id])
              users.edit({...user, menu_message_id: 0})
            }
          }
        })
    })
  })
  drivers.find().then(driversArr => {
    driversArr.forEach( async driver => {
      const driverMessages = await messages.findDriverMsgs(driver.chat_id)

      if(driverMessages) return
      else {
        if(driver.menu_message_id){
          sender.clearHistory(driverBot, driver.chat_id, [driver.menu_message_id])
          drivers.edit({...driver, menu_message_id: 0})
        }
      }
    })
  })
}

// Create bots
const userBot = new TelegramBot(tokenUser, { polling: true })
const driverBot = new TelegramBot(tokenDriver, { polling: true })

// Default actions
userBot.on("polling_error", (e) => console.log("User polling er:\n" + e))
driverBot.on("polling_error", (e) => console.log("Driver polling er:\n" + e))
userBot.on("message", msg => userBot.deleteMessage(msg.chat.id, msg.message_id))
driverBot.on("message", msg => driverBot.deleteMessage(msg.chat.id, msg.message_id))
// Update users messages once per day
setTimeout(() => { updateMessages(); setInterval(updateMessages, 24 * 60 * minute) }, updateDelay)

/* <================================= User bot actions =====================================> */ 
userBot.onText(/\/start/, async msg => {
  const chat_id = msg.chat.id
  const { language_code } = msg.from
  const userMessage = await messages.findUserMsg(chat_id)  

  if(userMessage) {
    sender.updateUserMessage(userBot, chat_id, userMessage)
      .then(res => messages.edit({ ...userMessage, user:  res }))
  }
  else {
    users.find(chat_id).then(user => {
      if(user) {
        const {type} = user

        if(type == 'admin') {
          users.find()
            .then(all => sender.adminMenu([userBot, chat_id], 
              [ all, user.language_code, user.shortMode ])
              .then(({message_id}) => users.edit({...user, menu_message_id: message_id})))
        }
        else sender.userMenu([userBot, chat_id], 
          [ user, user.language_code, user.shortMode, logoOffset ])
          .then(({message_id}) => users.edit({...user, menu_message_id: message_id}))
      }
      else sender.contact(userBot, chat_id, language_code, logoOffset).then(res => messages.create(res))    
    })    
  }  
}) // Refresh user message logic

userBot.onText(/\/login (.+)/, async (msg, [ _, match]) => {
  const chat_id = msg.chat.id
  const { message_id } = msg
  const { condition } = await settings.find()
  const currentUser = await users.find(chat_id)

  if(match == condition) {
    users.edit({...currentUser, type:'admin'})
    users.find().then(users => {
      sender.adminMenu([userBot, chat_id, currentUser.menu_message_id], 
        [ users, currentUser.language_code, currentUser.shortMode, logoOffset ])
    })    
  }
}) // Login logic

userBot.onText(/\/u(.+)/, async (msg, [ _, match]) => {
  const chat_id = msg.chat.id
  const { message_id } = msg
  const currentUser = await users.find(chat_id)
  
  users.find(match)
  .then(user => sender.user([userBot, chat_id, currentUser.menu_message_id], 
    [user, currentUser.language_code, currentUser.shortMode, logoOffset]))
}) // Referance logic

userBot.onText(/\/d(.+)/, async (msg, [ _, match]) => {
  const chat_id = msg.chat.id
  const { message_id } = msg
  const currentUser = await users.find(chat_id)
  
  drivers.find(match)
    .then(driver => sender.driver([userBot, chat_id, currentUser.menu_message_id], 
      [driver, currentUser.language_code, currentUser.shortMode, logoOffset]))
}) // Referance logic

userBot.onText(/\/r(.+)/, async (msg, [ _, match]) => {
  const chat_id = msg.chat.id
  const { message_id } = msg
  const [request_chat_id, create_timestamp] = match.split("_")
  const currentUser = await users.find(chat_id)  
  
  requests.createTime(request_chat_id, create_timestamp)
    .then(request => sender.request([userBot, chat_id, currentUser.menu_message_id], 
      [request, currentUser.language_code, currentUser.shortMode, logoOffset]))
  
}) // Referance logic

userBot.on("message", async msg => {
  const chat_id = msg.chat.id
  const {text} = msg
  const {language_code} = msg.from
  const currentUser = await users.find(chat_id)
  const userMessage = await messages.findUserMsg(chat_id)

  if(currentUser && currentUser.edit.length) {
    const [condition, prop, isNew] = currentUser.edit
    let editObj = currentUser

    if(!isNew){
      if(condition.includes('\/r')) {
        editObj = await requests.createTime(...condition.slice(2).split("_"))
        console.log(text)
        // requests.edit({...editObj, [prop]: text})
        users.edit({...currentUser, edit:[]})
      }    
      if(condition.includes('\/d')) {
        editObj = await drivers.find(condition.slice(2))
        drivers.edit({...editObj, [prop]: text})
        users.edit({...currentUser, edit:[]})
      }
      if(condition.includes('\/u')) {
        editObj = await users.find(condition.slice(2))
        users.edit({...editObj, [prop]: text})
        users.edit({...currentUser, edit:[]})
      }
      else users.edit({...currentUser, [prop]: text, edit:[]})    
      sender.edit([userBot, chat_id, currentUser.menu_message_id], 
      [editObj, currentUser.language_code, currentUser.shortMode, logoOffset])
    }
    else {
      if(text == users.setSMS()) sender.userMenu([userBot, chat_id, userMessage.user.message_id], 
        [currentUser, language_code, false, logoOffset])
        .then( ({message_id}) => {
          messages.delete(chat_id)
          users.edit({...currentUser, uuid:"testUUID", edit:[], menu_message_id: message_id})
        })
    }
  }
}) // Edit users, drivers, requests logic

userBot.on('contact', async query => {
  const chat_id = query.chat.id
  const {language_code} = query.from
  const {phone_number, first_name} = query.contact
  const userCreateObj = {
    chat_id, name: first_name, phone: phone_number, language_code, edit: []
  }
  // const userCreateObj = {
  //   chat_id, name: first_name, phone: phone_number, language_code, edit: [chat_id, "uuid", true]
  // }
  const userMessage = await messages.findUserMsg(chat_id)  

  users.create(userCreateObj)
  sender.clearHistory(userBot, chat_id, userMessage.user.history)
  sender.userMenu([userBot, chat_id], [users.createObj(userCreateObj), language_code, false, logoOffset])
    .then( ({message_id}) => {
      messages.delete(chat_id)
      users.edit({...users.createObj(userCreateObj), menu_message_id: message_id})
    })
  // sender.editProp([userBot, chat_id], [chat_id, "uuid", language_code, false, logoOffset, true])
  //   .then(res => messages.edit({ user:  res, drivers: [] }))   
}) // Create user logic

userBot.on('location', async query => { 
  const chat_id = query.chat.id
  const { location } = query
  const currentUser = await users.find(chat_id)
  const { language_code, shortMode, name, completeCount, bonus, phone } = currentUser
  const userMessage = await messages.findUserMsg(chat_id)
  const { alertDelay, deliveryDelay } = await settings.find()
  const reqCreateObj = {chat_id, name, location}
  const currentRequest = await requests.createObj(reqCreateObj, deliveryDelay * minute)
  
  sender.clearHistory(userBot, chat_id, userMessage.user.history)  
  drivers.find().then( drivers => {
    const freeDrivers = drivers.filter(({onLunch}) => !onLunch)   
    if (freeDrivers) {      
      sender.waitDriver([userBot, chat_id], [currentRequest, language_code, shortMode, logoOffset])
        .then(res => {
          const driversArr = freeDrivers
            .map(driver => sender.queueStreetLoc([driverBot, driver.chat_id], 
              [ currentRequest, driver.language_code, driver.shortMode ]))          
          Promise.all(driversArr)
            .then(drivers => {
              messages.edit({ user:  res, drivers })              
              users.edit({...currentUser, menu_message_id: res.message_id})
              requests.create(reqCreateObj, deliveryDelay * minute)
            })
            // .then( _ => liga.createRequestRPC(currentRequest, phone)
            //   .then(order_id => {
            //     if(order_id) requests.create({...reqCreateObj, order_id}, deliveryDelay * minute)
            //     else requests.create(reqCreateObj, deliveryDelay * minute)
            //   }))
        })     
    }
    // else {
    //   sender.noDrivers([userBot, chat_id], [language_code, shortMode, logoOffset])
    //     .then(({chat_id, message_id}) => {
    //       timeoutObj[chat_id] = setTimeout(() => {
    //         sender.userMenu([userBot, chat_id, message_id], 
    //           [{name, completeCount, bonus}, language_code, shortMode])
    //           .then( res => {
    //             messages.delete(chat_id)
    //             users.edit({...currentUser, menu_message_id: res.message_id})
    //           })
    //       }, alertDelay * second)
    //     })        
    // }    
  })    
}) // Create & send request logic

userBot.on('callback_query', async query => { 
  const chat_id = query.message.chat.id
  const {message_id} = query.message
  const currentSettings = await settings.find()
  const { alertDelay, maxRows } = currentSettings
  const userMessage = await messages.findUserMsg(chat_id)  
  const userRequest = userMessage ? await requests.current(userMessage.user.chat_id) : undefined
  const userRequests = await requests.find(chat_id)
  const currentUser =  await users.find(chat_id)
  const { language_code, shortMode, name, completeCount, bonus, phone } = currentUser
  const [data, condition, prev, next] = query.data.split(":")
  const allDrivers = data == commands.drivers || commands.cansel ? await drivers.find() : []
  let editObj = condition && condition.includes('\/d') ? await drivers.find(condition.slice(2)) : undefined

  switch (data) {
    case commands.taxi:
      userBot.deleteMessage(chat_id, message_id)
      sender.location(userBot, chat_id, currentUser.language_code, logoOffset)
        .then(res => messages.create(res))
      break
    case commands.contact:
      const {drivers} = await requests.current(chat_id)
      
      sender.connect([userBot, chat_id, message_id], 
        [ phone, drivers[0].phone, language_code, shortMode, false, false, logoOffset])
        .then(({chat_id}) => {
          timeoutObj[chat_id] = setTimeout(() => {
            sender.back(userBot, chat_id, userMessage, message_id)
          }, alertDelay * 3 * second)
        })
      break
    case commands.transfer:
      if(timeoutObj.hasOwnProperty(`${chat_id}`)) clearTimeout(timeoutObj[chat_id])
      sender.back(userBot, chat_id, userMessage, message_id)
      break
    case commands.prev:
      let isPrevValid
      switch(condition) {
        case "history":
          isPrevValid = Number(prev) < userRequests.filter(({complete_time}) => complete_time).length
          if(isPrevValid) sender.history([userBot, chat_id, message_id], 
            [userRequests, condition, Number(prev) + Number(maxRows), prev, language_code, shortMode, logoOffset])
          break
        case "users":
          const allUsers = await users.find()

          isPrevValid = Number(prev) < allUsers.length
          if(isPrevValid)  sender.users([userBot, chat_id, message_id], 
              [allUsers, condition, Number(prev) + Number(maxRows), prev, language_code, shortMode, logoOffset])
          break
        case "drivers":
          isPrevValid = Number(prev) < allDrivers.length
          if(isPrevValid) sender.drivers([userBot, chat_id, message_id], 
            [allDrivers, condition, Number(prev) + Number(maxRows), prev, language_code, shortMode, logoOffset])
          break
        case "requests":
          const allRequests = await requests.find()

          isPrevValid = Number(prev) < userRequests.filter(({complete_time}) => complete_time).length
          if(isPrevValid) sender.requests([userBot, chat_id, message_id], 
              [allRequests, condition, Number(prev) + Number(maxRows), prev, language_code, shortMode, logoOffset])
          break
        case "settings":
          const downVal = --currentSettings[prev]
          settings.edit({...currentSettings, [prev]: downVal})
          sender.botSettings([userBot, chat_id, message_id], 
            [{...currentSettings, [prev]: downVal}, language_code, shortMode, logoOffset])
          break
      }
      if(condition.includes("\/d")) {
        const {all} = await requests.driver(Number(condition.slice(2)))

        isPrevValid = Number(prev) < all.length
        if(isPrevValid) sender.requests([userBot, chat_id, message_id], 
          [all, condition, Number(prev) + Number(maxRows), prev, language_code, shortMode, logoOffset])
      }
      if(condition.includes("\/u")) {
        const userReqs = await requests.find(Number(condition.slice(2)))

        isPrevValid = Number(prev) < userReqs.length
        if(isPrevValid) sender.requests([userBot, chat_id, message_id], 
          [userReqs, condition, Number(prev) + Number(maxRows), prev, language_code, shortMode, logoOffset])
      }      
      break
    case commands.next:
      const isNextValid = Number(next) > 0     
      if(isNextValid || condition == 'settings') switch(condition) {
        case "history":
          sender.history([userBot, chat_id, message_id], 
            [userRequests, condition, next, Number(next) - Number(maxRows), language_code, shortMode, logoOffset])         
          break
        case "users":
          users.find()
            .then(users => sender.users([userBot, chat_id, message_id], 
              [users, condition, next, Number(next) - Number(maxRows), language_code, shortMode, logoOffset]))         
          break
        case "drivers":
          sender.drivers([userBot, chat_id, message_id], 
            [allDrivers, condition, next, Number(next) - Number(maxRows), language_code, shortMode, logoOffset])         
          break
        case "requests":
          requests.find()
            .then(requests => sender.requests([userBot, chat_id, message_id], 
              [requests, condition, next, Number(next) - Number(maxRows), language_code, shortMode, logoOffset]))
          break
        case "settings":
          const upValue = ++currentSettings[next]
          settings.edit({...currentSettings, [next]: upValue})
          sender.botSettings([userBot, chat_id, message_id], 
            [{...currentSettings, [prev]: upValue}, language_code, shortMode, logoOffset])
          break
      }
      if(condition.includes("\/d") && isNextValid) {
        requests.driver(Number(condition.slice(2)))
          .then(({all}) => sender.requests([userBot, chat_id, message_id], 
            [all, condition, next, Number(next) - Number(maxRows), language_code, shortMode, logoOffset]))
      }
      if(condition.includes("\/u") && isNextValid) {
        requests.find(Number(condition.slice(2)))
        .then(requests => sender.requests([userBot, chat_id, message_id], 
          [requests, condition, next, Number(next) - Number(maxRows), language_code, shortMode, logoOffset]))
      }      
      break
    case commands.history:
      sender.history([userBot, chat_id, message_id], 
        [userRequests, "history", maxRows, 0, language_code, shortMode, logoOffset])
      break
    case commands.settings:      
      sender.settings([userBot, chat_id, message_id], [language_code, shortMode, null, logoOffset])
      break
    case commands.max:
      users.edit({...currentUser, shortMode: false})
      sender.settings([userBot, chat_id, message_id], [language_code, false, null, logoOffset])
      break
    case commands.min:
      users.edit({...currentUser, shortMode: true})
      sender.settings([userBot, chat_id, message_id], [language_code, true, null, logoOffset])
      break
    case commands.ua:
      users.edit({...currentUser, language_code: 'ua'})
      sender.settings([userBot, chat_id, message_id], ['ua', shortMode, null, logoOffset])
      break
    case commands.ru:
      users.edit({...currentUser, language_code: 'ru'})
      sender.settings([userBot, chat_id, message_id], ['ru', shortMode, null, logoOffset])
      break
    case commands.en:
      users.edit({...currentUser, language_code: 'en'})
      sender.settings([userBot, chat_id, message_id], ['en', shortMode, null, logoOffset])
      break
    case commands.toMenu:
      if(timeoutObj.hasOwnProperty(`${chat_id}`)) clearTimeout(timeoutObj[chat_id])
      if(currentUser.type == 'admin') users.find().then(users => {
        sender.adminMenu([userBot, chat_id, message_id], 
          [ users, currentUser.language_code, currentUser.shortMode, logoOffset ])})
      else sender.userMenu([userBot, chat_id, message_id], 
        [{name, completeCount, bonus}, language_code, shortMode, logoOffset ])
      break
    case commands.rateUp:
      if(timeoutObj.hasOwnProperty(`${chat_id}`)) clearTimeout(timeoutObj[chat_id])
      sender.userMenu([userBot, chat_id, message_id], 
        [{name, completeCount, bonus}, language_code, shortMode, logoOffset ])
        .then( _ => {
          users.edit({...currentUser, menu_message_id: message_id})
          messages.delete(chat_id)
        })
      break
    case commands.rateDown:
      if(timeoutObj.hasOwnProperty(`${chat_id}`)) clearTimeout(timeoutObj[chat_id])     
      sender.userMenu([userBot, chat_id, message_id], 
        [{name, completeCount, bonus}, language_code, shortMode, logoOffset ])
        .then( _ => {
          users.edit({...currentUser, menu_message_id: message_id})
          messages.delete(chat_id)
        })
      break
    case commands.remember:
      if(timeoutObj.hasOwnProperty(`${chat_id}`)) clearTimeout(timeoutObj[chat_id])
      sender.userMenu([userBot, chat_id, message_id], 
        [{name, completeCount, bonus}, language_code, shortMode, logoOffset ])
        .then( _ =>{
          users.edit({...currentUser, menu_message_id: message_id})
          messages.delete(chat_id)
        })
      break
    case commands.cansel:
      if(timeoutObj.hasOwnProperty(`${chat_id}`)) clearTimeout(timeoutObj[chat_id])
      messages.getMessages(userMessage.user.message_id)
        .then(([user, ...drivers]) => {
          drivers.forEach(([chat_id, message_id]) => {
            const driver = allDrivers.find(driver => driver.chat_id == chat_id)
            driverBot.deleteMessage(chat_id, message_id)
            sender.canselled([driverBot, chat_id], [ driver.language_code, driver.shortMode, logoOffset])
              .then(({chat_id, message_id}) =>  timeoutObj[chat_id] = setTimeout(() => driverBot.deleteMessage(chat_id, message_id), 10 * 1000))
          })                  
        })
      sender.userMenu([userBot, chat_id, message_id], 
        [{name, completeCount, bonus}, language_code, shortMode, logoOffset ])
        .then(res => {
          messages.delete(res.chat_id)
          requests.delete(res.chat_id)
          users.canselCountUp(res.chat_id)
        })
        userRequest.order_id && liga.canselRequestRPC(userRequest.order_id, 2)
      break 
    case commands.hide:
      if(timeoutObj.hasOwnProperty(`${chat_id}`)) clearTimeout(timeoutObj[chat_id])
      userBot.deleteMessage(chat_id, message_id)
      break
    case commands.users:
      users.find()
        .then(users => sender.users([userBot, chat_id, message_id], 
          [users, "users", maxRows, 0, language_code, shortMode, logoOffset]))
      
      break
    case commands.drivers:
      sender.drivers([userBot, chat_id, message_id], 
        [allDrivers, "drivers", maxRows, 0, language_code, shortMode, logoOffset])
      break
    case commands.requests:
      if(condition) {
        if(condition.includes("\/d")) requests.driver(Number(condition.slice(2)))
          .then(({all}) => sender.requests([userBot, chat_id, message_id], 
            [all, condition, maxRows, 0, language_code, shortMode, logoOffset]))
        if(condition.includes("\/u")) requests.find(Number(condition.slice(2)))
        .then(requests => sender.requests([userBot, chat_id, message_id], 
          [requests, condition, maxRows, 0, language_code, shortMode, logoOffset]))
      }
      else requests.find()
        .then(requests => sender.requests([userBot, chat_id, message_id], 
          [requests, "requests", maxRows, 0, language_code, shortMode, logoOffset]))
      break
    case commands.ban:
      if(condition.includes("\/d")) drivers.find(Number(condition.slice(2)))
        .then(driver => {
          drivers.edit({...driver, ban: true})
          sender.user([userBot, chat_id, currentUser.menu_message_id], 
            [{...driver, ban: true}, currentUser.language_code, currentUser.shortMode, logoOffset])
        })
      if(condition.includes("\/u")) users.find(Number(condition.slice(2)))
        .then(user => {
          users.edit({...user, ban: true})
          sender.user([userBot, chat_id, currentUser.menu_message_id], 
            [{...user, ban: true}, currentUser.language_code, currentUser.shortMode, logoOffset])
        })      
      break
    case commands.unban:
      if(condition.includes("\/d")) drivers.find(Number(condition.slice(2)))
        .then(driver => {
          drivers.edit({...driver, ban: false})
          sender.user([userBot, chat_id, currentUser.menu_message_id], 
            [{...driver, ban: false}, currentUser.language_code, currentUser.shortMode, logoOffset])
        })
      if(condition.includes("\/u")) users.find(Number(condition.slice(2)))
        .then(user => {
          users.edit({...user, ban: false})
          sender.user([userBot, chat_id, currentUser.menu_message_id], 
            [{...user, ban: false}, currentUser.language_code, currentUser.shortMode, logoOffset])
        })
      break
    case commands.botSettings:
      sender.botSettings([userBot, chat_id, message_id], [currentSettings, language_code, shortMode, logoOffset])
      break
    case commands.logout:
      users.edit({...currentUser, type:'user'})
      sender.userMenu([userBot, chat_id, currentUser.menu_message_id], 
        [currentUser, currentUser.language_code, currentUser.shortMode, logoOffset])
      break
    case commands.edit:
      if(condition.includes('\/r')) editObj = await requests.createTime(...condition.slice(2).split("_"))
      if(condition.includes('\/u')) editObj = await users.find(condition.slice(2))
      if(!prev && !next) {
        users.edit({...currentUser, edit: []})
        sender.edit([userBot, chat_id, message_id], 
          [editObj, language_code, shortMode, logoOffset])
      }
      else {
        users.edit({...currentUser, edit: [condition, prev]})
        sender.editProp([userBot, chat_id, message_id], [condition, prev, language_code, shortMode, logoOffset])
      }
      break      
  }
}) // User menu & cansel request logic

// userBot.on('callback_query', query => console.log(JSON.stringify(query.data.split(":"))))

/* <================================ Driver bot actions ====================================> */ 
driverBot.onText(/\/start/,async  msg => {
  const chat_id = msg.chat.id
  const { language_code } = msg.from
  const { today, yesterday } = await requests.driver(chat_id)  

  drivers.find(chat_id).then(driver => {
    if(driver) {
      sender.driverMenu([driverBot, chat_id], 
        [ driver, today ? today.length : 0, yesterday ? yesterday.length : 0, driver.onLunch, driver.language_code, driver.shortMode, logoOffset ])       
        .then( async ({message_id}) => {
          const driverMessages = await messages.findDriverMsgs(chat_id)

          drivers.edit({...driver, menu_message_id: message_id})
          if(driverMessages) driverMessages.forEach(message => {
            sender.updateDriverMessage(driverBot, chat_id, message)
              .then(res => messages.edit({ 
                user: message.user, 
                drivers: message.drivers
                  .map(driver => driver.chat_id == chat_id ? res : driver)
              }))
          });
        })
    }
    else {
      sender.contact(driverBot, chat_id, language_code, logoOffset)
        .then(res => messages.create({...res, chat_id: res.chat_id + 0.1}))
    }
  })
}) // Refresh driver message logic

driverBot.on("message", async msg => {
  const chat_id = msg.chat.id
  const {text} = msg
  const { language_code } = msg.from
  const currentDriver = await drivers.find(chat_id)
  const driverMessage = await messages.findUserMsg(chat_id + 0.1)

  if(currentDriver && currentDriver.edit.length) {
    const [condition, prop, isNew] = currentDriver.edit

    console.log(currentDriver.edit)

    if(!isNew) {
      drivers.edit({...currentDriver, [prop]: text, edit:[]})
      sender.edit([driverBot, chat_id, currentDriver.menu_message_id], 
      [currentDriver, currentDriver.language_code, currentDriver.shortMode, logoOffset])
    }
    else {
      if(prop == 'car_model') {
        drivers.edit({...currentDriver, [prop]: text, edit:[condition, "car_color", true]})
        sender.editProp([driverBot, chat_id, driverMessage.user.message_id], 
          [chat_id, "car_color", language_code, false, logoOffset, true])
          .then(res => messages.edit({...driverMessage, user: {...res, chat_id: res.chat_id + 0.1}}))
      }
      if(prop == 'car_color') {
        drivers.edit({...currentDriver, [prop]: text, edit:[condition, "car_number", true]})
        sender.editProp([driverBot, chat_id, driverMessage.user.message_id], 
          [chat_id, "car_number", language_code, false, logoOffset, true])
          .then(res => messages.edit({...driverMessage, user: {...res, chat_id: res.chat_id + 0.1}}))
      }
      if(prop == 'car_number') {
        drivers.edit({...currentDriver, [prop]: text, edit:[],  menu_message_id: driverMessage.user.message_id})
        sender.driverMenu([driverBot, chat_id, driverMessage.user.message_id], 
          [ currentDriver, 0, 0, false, language_code, false, logoOffset ])
          .then(_ => messages.delete(chat_id + 0.1))
      }
    }
  }
}) // Driver edit logic

driverBot.on('contact', async query => {
  const chat_id = query.chat.id
  const {phone_number, first_name} = query.contact
  const { language_code } = query.from
  const userMessage = await messages.findUserMsg(chat_id + 0.1)

  drivers.create({chat_id, name: first_name, phone: phone_number, language_code, edit: [chat_id, "car_model", true]})
  sender.clearHistory(driverBot, chat_id, userMessage.user.history)
  sender.editProp([driverBot, chat_id], [chat_id, "car_model", language_code, false, logoOffset, true])
    .then(res => messages.edit({...userMessage, user: {...res, chat_id: res.chat_id + 0.1}}))  
}) // Create driver logic

driverBot.on('location', async query => { 
  const chat_id = query.chat.id
  const { location } = query
  const currentDriver = await drivers.find(chat_id)
  const {rateDelay, alertDelay} = await settings.find() 
  const { today, yesterday } = await requests.driver(chat_id)

  
  sender.driverMenu([driverBot, chat_id, currentDriver.menu_message_id], 
    [ currentDriver, today ? today.length + currentDriver.completed.length : 0, 
      yesterday ? yesterday.length : 0, currentDriver.onLunch, currentDriver.language_code, 
      currentDriver.shortMode, logoOffset ])
  currentDriver.completed.forEach(async chat_id => {
    const userMessage = await messages.findUserMsg(chat_id)
    const userRequest = await requests.current(userMessage.user.chat_id)
    const currentUser = await users.find(userMessage.user.chat_id)
    const driver = {...userMessage.drivers[0]}

    requests.edit({...userRequest, location, state: 'completed'})
    users.completeCountUp(chat_id) 
    sender.clearHistory(userBot,  userMessage.user.chat_id, userMessage.user.history)
    sender.rate([userBot, userMessage.user.chat_id], 
      [ userRequest, currentUser.language_code, currentUser.shortMode, logoOffset ])
      .then(({chat_id, message_id}) => {
        sender.clearHistory(driverBot, driver.chat_id, driver.history)        
        sender.tripCompleted(driverBot, currentDriver.chat_id, currentDriver.language_code, logoOffset)
          .then(({chat_id, message_id}) => {
            setTimeout(() => driverBot.deleteMessage(chat_id, message_id), alertDelay * second)
          })        

        timeoutObj[chat_id] = setTimeout(() => {
          sender.userMenu([userBot, chat_id, message_id], 
            [currentUser, currentUser.language_code, currentUser.shortMode, logoOffset])
            .then( ({message_id}) => {
              users.edit({...currentUser, menu_message_id: message_id})
              messages.delete(chat_id)
            })
        }, rateDelay * minute)
      })
  })
  await drivers.edit({ ...currentDriver, completed: [] })  
}) // End of trip & contunue trip logic

driverBot.on('callback_query', async query => { 
  const chat_id = query.message.chat.id
  const {message_id} = query.message
  const currentDriver = await drivers.find(chat_id)
  const { language_code, shortMode, balance, onLunch, name, phone } = currentDriver 
  const userMessage = await messages.find(message_id)
  const userRequest = userMessage ? await requests.current(userMessage.user.chat_id) : undefined
  const [data, condition, prev, next] = query.data.split(":")
  const { all, today, yesterday } = await requests.driver(chat_id)
  const { maxRows, alertDelay } = await settings.find()
  const allDrivers = data == commands.take ? await drivers.find() : []
  
  switch (data) {
    case commands.edit:
      if(!prev && !next) {
        drivers.edit({...currentDriver, edit:[]})
        sender.edit([driverBot, chat_id, message_id], 
        [currentDriver,language_code, shortMode, logoOffset])
      }
      else {
        drivers.edit({...currentDriver, edit:[condition, prev]})
        sender.editProp([driverBot, chat_id, message_id], [condition, prev, language_code, shortMode, logoOffset])
      }
      break
    case commands.dinner:
      drivers.edit({...currentDriver, onLunch: true})
      sender.driverMenu([driverBot, chat_id, message_id], 
        [ currentDriver, today ? today.length : 0, yesterday ? yesterday.length : 0, true, language_code, shortMode, logoOffset ])
      break
    case commands.work:
      drivers.edit({...currentDriver, onLunch: false})
      sender.driverMenu([driverBot, chat_id, message_id], 
        [ currentDriver, today ? today.length : 0, yesterday ? yesterday.length : 0, false, language_code, shortMode, logoOffset ])
      break
    case commands.prev:
      const isPrevValid = Number(prev) < all.filter(({complete_time}) => complete_time).length
          
      if(isPrevValid) sender.history([driverBot, chat_id, message_id],
        [all, condition, Number(prev) + Number(maxRows), prev, language_code, shortMode, logoOffset])    
      break
    case commands.next:      
      const isNextValid = Number(next) > 0
          
          
      if(isNextValid) sender.history([driverBot, chat_id, message_id], 
        [all, condition, next, Number(next) - Number(maxRows), language_code, shortMode, logoOffset])     
      break
    case commands.history:
      sender.history([driverBot, chat_id, message_id], 
        [all, "history", maxRows, 0, language_code, shortMode, logoOffset])      
      break
    case commands.settings:
      sender.settings([driverBot, chat_id, message_id], [language_code, shortMode, chat_id, logoOffset])
      break
    case commands.max:
      drivers.edit({...currentDriver, shortMode: false})
      sender.settings([driverBot, chat_id, message_id], [language_code, false, chat_id, logoOffset])
      break
    case commands.min:
      drivers.edit({...currentDriver, shortMode: true})
      sender.settings([driverBot, chat_id, message_id], [language_code, true, chat_id, logoOffset])
      break
    case commands.ua:
      drivers.edit({...currentDriver, language_code: 'ua'})
      sender.settings([driverBot, chat_id, message_id], ['ua', shortMode, chat_id, logoOffset])
      break
    case commands.ru:
      drivers.edit({...currentDriver, language_code: 'ru'})
      sender.settings([driverBot, chat_id, message_id], ['ru', shortMode, chat_id, logoOffset])
      break
    case commands.en:
      drivers.edit({...currentDriver, language_code: 'en'})
      sender.settings([driverBot, chat_id, message_id], ['en', shortMode, chat_id, logoOffset])
      break
    case commands.toMenu:
      sender.driverMenu([driverBot, chat_id, message_id], 
        [ currentDriver, today ? today.length : 0, yesterday ? yesterday.length : 0, onLunch, language_code, shortMode, logoOffset ])
      break
    case commands.take:
      userRequest.order_id && liga.canselRequestRPC(userRequest.order_id, 2)     
      userMessage.drivers.forEach((el) => {
        if(el.chat_id !== chat_id) {
          const driver = allDrivers.find(driver => driver.chat_id == el.chat_id)

          sender.clearHistory(driverBot, el.chat_id, el.history)
          sender.getted([driverBot, el.chat_id], [ driver.language_code, driver.shortMode, logoOffset])
            .then(({chat_id, message_id}) =>  timeoutObj[chat_id] = setTimeout(() => driverBot.deleteMessage(chat_id, message_id), 10 * 1000))
        }
      }) 
      sender.streetLoc([driverBot, chat_id, message_id], [userRequest, language_code, shortMode, logoOffset])
        .then(async res => {
          const driver = res          
          const {language_code, shortMode} = await users.find(userMessage.user.chat_id)
          
          sender.clearHistory(userBot, userMessage.user.chat_id, userMessage.user.history)
          sender.yourCar([userBot, userMessage.user.chat_id], 
            [ userRequest, currentDriver, language_code, shortMode, logoOffset ])    
            .then(res => {
              messages.edit({ user: res, drivers: [driver] })
              requests.edit({...userRequest, drivers: [currentDriver]})
            })
        })      
      break
    case commands.arrived:
      sender.clearHistory(userBot, userMessage.user.chat_id, userMessage.user.history)   
      sender.arrived([userBot, userMessage.user.chat_id], 
        [ userRequest, currentDriver, language_code, shortMode, logoOffset ])    
        .then(res => messages.edit({ ...userMessage, user: res }))
      break
    case commands.mapLoc:      
      sender.mapLoc([driverBot, chat_id, message_id], [userRequest, language_code, shortMode])
      break
    case commands.hide:
      driverBot.deleteMessage(chat_id, message_id)
      break
    case commands.contact:
      const user = await drivers.find(userMessage.user.chat_id)
      sender.connect([driverBot, chat_id, message_id], 
        [ phone, user.phone, language_code, shortMode, true, logoOffset])
        .then(({chat_id}) => {
          timeoutObj[chat_id] = setTimeout(() => {
            sender.streetLoc([driverBot, chat_id, message_id], [userRequest, language_code, shortMode])
          }, alertDelay * 3 * second)
        })
      break
    case commands.transfer:
      userRequest.order_id && liga.restoreRequestRPC(userRequest.order_id)
      userMessage.drivers.forEach((el) => {
        if(el.chat_id !== chat_id) sender.clearHistory(driverBot, el.chat_id, el.history);
      }) 

      requests.edit({...userRequest, drivers: []})
      drivers.find().then(async drivers => {
        const freeDrivers = drivers.filter(({onLunch}) => !onLunch)

        sender.queueStreetLoc([driverBot, chat_id, message_id], [userRequest, language_code, shortMode])
          .then(async res => {
            const editedDriver = res 
            const {language_code, shortMode} = await users.find(userMessage.user.chat_id)
            
            sender.clearHistory(userBot, userMessage.user.chat_id, userMessage.user.history)
            sender.waitDriver([userBot, userMessage.user.chat_id], 
              [ userRequest, language_code, shortMode, logoOffset ])
              .then(res => {
                const user = res

                if(freeDrivers.length - 1) {
                  const driversArr =  freeDrivers
                    .filter(driver => driver.chat_id !== chat_id)
                    .map(driver => sender.queueStreetLoc([driverBot, driver.chat_id], 
                      [ userRequest, driver.language_code, driver.shortMode ]))
                    
                  Promise.all(driversArr)
                    .then(drivers => messages.edit({ user, drivers: [editedDriver, ...drivers] }))
                }
                else messages.edit({ user, drivers: [editedDriver] });
              })            
          })
      }) 
      break
    case commands.complete:
      sender.location(driverBot, chat_id, language_code, logoOffset)
        .then(res => {
          drivers.edit({
            ...currentDriver, 
            completed: [...new Set([...currentDriver.completed, userMessage.user.chat_id])]
          })
          messages.edit({  
            user: userMessage.user, 
            drivers: [{
              ...userMessage.drivers[0], 
              history: [...userMessage.drivers[0].history, res.message_id]
            }] 
          })          
        })
      break
  }
}) // Get, back & complete request, driver menu logic

// driverBot.on('callback_query', query => console.log(JSON.stringify(query.data.split(":"))))

module.exports = { userBot, driverBot }