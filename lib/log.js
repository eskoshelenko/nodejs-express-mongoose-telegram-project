const { open, close, appendFile } = require('fs')

class Log {
  constructor(path) {
    this.path = path.split("\\").join('/')
  }

  updateObj (obj) {
    const newObj = {}

    for(let prop in obj) {
      if(obj[prop].toString().length < 2) {
        newObj[prop] = `0${obj[prop]}`
      }
      else {
        newObj[prop] = obj[prop]
      }
    }

    return newObj
  }

  fileName() {
    const date = new Date()

    const d = this.updateObj({
      year: date.getFullYear(),
      month: date.getMonth() + 1,
      day: date.getDate()
    })
    
    return `${d.year}-${d.month}-${d.day}.log`   
  }

  time() {
    const date = new Date()

    const d = this.updateObj({
      hours: date.getHours(),
      minites: date.getMinutes(),
      seconds: date.getSeconds()
    })
    
    return `${d.hours}:${d.minites}:${d.seconds}/`   
  }

  closeFile(fd) {
    close(fd, (e) => {
      if (e) console.log(`File ${this.path}/${this.fileName()} close failed:\n${e.message}`)
    })
  }

  save(data) {
    open(`${this.path}/${this.fileName()}`, 'a', (e, fd) => {
      if (e) console.log(`File ${this.path}/${this.fileName()} open failed:\n${e.message}`)
      console.log(this.time() + data)
      try {
        appendFile(fd, this.time() + data + '\n', 'utf8', (e) => {
          this.closeFile(fd)
          if (e) console.log(`File ${this.path}/${this.fileName()} append failed:\n${e.message}`)
        })
      } catch (e) {
        this.closeFile(fd)
        console.log(`File ${this.path}/${this.fileName()} error occured:\n${e.message}`)
      }
    })
  }
}

module.exports = { Log }

