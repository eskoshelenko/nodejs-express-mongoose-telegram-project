const buttons = {
  getCommands() {
    const commands = {}
    const charCodeToStr = str => {
      return str.toString().split("").map(el => el.charCodeAt()).join("")
    }

    Object.entries(this)
      .filter(([_, val]) => val.hasOwnProperty('icon'))
      .forEach(([key, val]) => commands[key] = charCodeToStr(val.icon))

    return commands
  },
  taxi: {
    icon:"\uD83D\uDE96",
    label: {
      ru: 'Такси',
      ua: 'Таксi',
      en: 'Taxi'
    }
  },
  history: {
    icon:"\uD83D\uDCD6",
    label: {
      ru: 'История',
      ua: 'Iсторiя',
      en: 'History'
    }
  },  
  settings: {
    icon:"\uD83D\uDD27",
    label: {
      ru: 'Настройки',
      ua: 'Налаштування',
      en: 'Settings'
    }
  },
  contact: {
    icon:"\uD83D\uDCDE",
    label: {
      ru: 'Связаться',
      ua: "Зв'язатися",
      en: 'Contact'
    }
  },  
  cansel: {
    icon:"\u274C",
    label: {
      ru: 'Отменить',
      ua: 'Вiдмiнити',
      en: 'Cansel'
    }
  },  
  take: {
    icon:"\u2714",
    label: {
      ru: 'Принять',
      ua: 'Прийняти',
      en: 'Take'
    }
  },
  arrived: {
    icon:"\uD83D\uDCAC",
    label: {
      ru: 'Прибыл',
      ua: 'Прибув',
      en: 'Arrived'
    }
  },
  mapLoc: {
    icon:"\uD83C\uDF04",
    label: {
      ru: 'На карте',
      ua: 'На карті',
      en: 'At map'
    }
  },
  streetLoc: {
    icon:"\uD83D\uDD21",
    label: {
      ru: 'К названю',
      ua: 'До назви',
      en: 'To the name'
    }
  },  
  edit: {
    icon:"\u270F",
    label: {
      ru: 'Редактировать',
      ua: 'Редагувати',
      en: 'Edit'
    }
  },  
  complete: {
    icon:"\uD83C\uDFC1",
    label: {
      ru: 'Завершить',
      ua: 'Завершити',
      en: 'Complete'
    }
  },  
  transfer: {
    icon:"\uD83D\uDD19",
    label: {
      ru: 'Вернуть',
      ua: 'Повернути',
      en: 'Return'
    }
  },  
  rateUp: {
    icon:"\uD83D\uDE00",
    label: {
      ru: 'Понравилось',
      ua: 'Сподобалося',
      en: 'Like'
    }
  },  
  rateDown: {
    icon:"\uD83D\uDE08",
    label: {
      ru: 'Не понравилось',
      ua: 'Не сподобалося',
      en: 'Dislike'
    }
  },
  remember: {
    icon:"\u2795",
    label: {
      ru: 'Добавить',
      ua: 'Додати',
      en: 'Add'
    }
  },
  dinner: {
    icon:"\uD83C\uDF5C",
    label: {
      ru: 'На обед',
      ua: 'На обiд',
      en: 'On dinner'
    }
  },  
  work: {
    icon:"\uD83D\uDE95",
    label: {
      ru: 'К работе',
      ua: 'До роботи',
      en: 'To work'
    }
  }, 
  toMenu: {
    icon:"\u2630",
    label: {
      ru: 'В Меню',
      ua: 'До Меню',
      en: 'To Menu'
    }
  },  
  next: {
    icon:"\u23E9",
    label: {
      ru: '',
      ua: '',
      en: ''
    }
  },
  prev: {
    icon:"\u23EA",
    label: {
      ru: '',
      ua: '',
      en: ''
    }
  },
  hide: {
    icon:"\u2796",
    label: {
      ru: 'Скрыть',
      ua: 'Приховати',
      en: 'Hide'
    }
  },  
  en: {
    icon:"ENG",
    command: "ENG:РУС:УКР",
    label: {
      ru: 'Lang.',
      ua: 'Lang.',
      en: 'Lang.'
    }
  },
  ua: {
    icon:"УКР",
    command: "УКР:ENG:РУС",
    label: {
      ru: 'Мова',
      ua: 'Мова',
      en: 'Мова'
    }
  },
  ru: {
    icon:"РУС",
    command: "РУС:УКР:ENG",
    label: {
      ru: 'Язык',
      ua: 'Язык',
      en: 'Язык'
    }
  },
  min: {
    icon:"\u27A1\u2B05",
    label: {
      ru: 'Мин. режим',
      ua: 'Мiн. режим',
      en: 'Short mode'
    }
  },
  max: {
    icon:"\u2B05\u27A1",
    label: {
      ru: 'Разш. режим',
      ua: 'Розш. режим',
      en: 'Wide mode'
    }
  },
  users: {
    icon:"\uD83D\uDC64",
    label: {
      ru: 'Пользователи',
      ua: 'Користувачі',
      en: 'Users'
    }
  },
  drivers: {
    icon:"\uD83D\uDC54",
    label: {
      ru: 'Водители',
      ua: 'Водії',
      en: 'Drivers'
    }
  },
  requests: {
    icon:"\uD83D\uDCC4", 
    label: {
      ru: 'Заявки',
      ua: 'Заявки',
      en: 'Requests'
    }
  },
  ban: {
    icon:"\uD83D\uDD07",
    label: {
      ru: 'Блокировать',
      ua: 'Блокувати',
      en: 'Block'
    }
  },
  unban: {
    icon:"\uD83D\uDD09",
    label: {
      ru: 'Разблокировать',
      ua: 'Розблокувати',
      en: 'Unblock'
    }
  },
  logout: {
    icon:"\uD83D\uDEAA",
    label: {
      ru: 'Выйти',
      ua: 'Вийти',
      en: 'Exit'
    }
  },
  botSettings: {
    icon:"\uD83D\uDE80",
    label: {
      ru: 'Настр. бота',
      ua: 'Налашт. боту',
      en: 'Bot settings'
    }
  },
  info: {
    icon:"\u2139",
    label: {
      ru: 'О нас',
      ua: 'Про нас',
      en: 'About us'
    }
  },
  send: {
    icon:"\uD83D\uDCE8",
    label: {
      ru: 'Отзыв',
      ua: 'Вiдгук',
      en: 'Review'
    }
  }
}

const commands = buttons.getCommands()

module.exports = {buttons, commands}