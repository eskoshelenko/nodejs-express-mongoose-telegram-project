const fs = require('fs')
const { path } = require('../config')
const { exec } = require("child_process")
const { Log } = require('./log')
const log = new Log(path + '/log/liga')

const settingsObj = {
  data: `Channel: Local/{client_phone}@from-internal\n` 
  + `CallerID: "autoinfo" <test>\n` 
  + `MaxRetries: 0\n` 
  + `RetryTime: 30\n` 
  + `WaitTime: 40\n` 
  + `Context: autoinfo\n` 
  + `Extension: info\n` 
  + `Priority: 1`,
  filePath: path.split("\\").join('/') + `/outgoing`,
  asteriskPath: `/var/spool/asterisk/outgoing`,
  asteriskIP: `<AsteriskIP>`,
  asteriskPWD: `AsteriskPWD`,
  bashCmnd(fileName) {
    return `sshpass -p '${this.asteriskPWD}' scp ${this.filePath}/${fileName} root@${this.asteriskIP}:${this.asteriskPath}`
  }
}

class CallFile {
  constructor(obj) {
   this.obj = obj
  }

  createFile(data, name) {
    fs.appendFile(this.obj.filePath + '/' + name, data, err => {
      if (err) {
        log.save(`File create ERROR: ${err.message}`)
        return false          
      }
    })  
  }

  async sendFile(client_phone) {
    const data = this.obj.data
      .replace(/{client_phone}/, client_phone.toString().slice(-10))
    const fileName = Date.now() + `.call`
    
    await this.createFile(data, fileName)
    
    exec(this.obj.bashCmnd(fileName), err => {
      if (err) log.save(`File send ERROR: ${err.message}`)
      log.save(`CallTo: { userPhone: ${client_phone}, reason: AUTOINFO }`)
    })    
  }
}

module.exports =  new CallFile(settingsObj)