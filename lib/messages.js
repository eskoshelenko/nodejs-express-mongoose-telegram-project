const { buttons, commands } = require("./buttons")
const { path } = require('../config')
const { Log } = require('./log')
const asterisk = require('./asteriskContact')
const log = new Log(path + '/log/bot')
const icon = require("./icons")

class Messages {
  constructor(defaultLang, logo, shortMode) {
    this.logo = logo || ''
    this.langArr = ['ru', 'ua', 'en']
    this.defaultLang = this.langArr.includes(defaultLang) ? defaultLang : 'en'
    this.shortMode = shortMode || false    
  }  

  setTimestamp(strDate) {    
    return new Date(strDate).valueOf()
  }

  formatLoc({lat, lng}) {
    return {latitude: lat, longitude: lng}
  }

  updateCommand(arr) {
    return arr.map(el => ":" + el).join("")
  }

  strToCharCode(str) {
    return str.toString().split("").map(el => el.charCodeAt()).join("")
  }

  setLang (lang) {
    return this.langArr.includes(lang) || !lang ? lang : this.defaultLang
  }

  logoOffset(offset) {
    const s = ` `
    if(offset && this.logo) return this.logo.split('\n')
      .filter(el => el)
      .map(el => s.repeat(offset) + el).join("\n") + "\n"
    else return ''
  }

  randomIcon(arr) {
    return arr[Math.floor(Math.random() * arr.length)]
  }

  /* <========================================== Icons methods =============================================>*/
  carRidesTo(logoOffset) {
    const arr = []
    for(let prop in icon) {
      if(prop.includes("car")) arr.push(icon[prop])
    }
    const setIcon = this.randomIcon(arr)
    const repeat = logoOffset || 10

    return `${icon.house + (logoOffset == 1 ?  icon.collision : "")}${" ".repeat(repeat)}${setIcon}${logoOffset == 1 ? "" : icon.dash}`
  }

  hearts(val) {
    const arr = []
    for(let prop in icon) {
      if(prop.includes("heart")) arr.push(icon[prop])
    }
    const setIcon = this.randomIcon(arr)

    return `${setIcon} <b>x ${val}</b>`
  }

  money(val) {
    return `${icon.moneyBag} <b>x ${val}</b>`
  }

  cars(str) {
    const arr = []
    for(let prop in icon) {
      if(prop.includes("car")) arr.push(icon[prop])
    }
    const setIcon = this.randomIcon(arr)

    return `${setIcon} ${str}`
  }

  crowns(str) {
    return `${icon.crown} ${str} ${icon.crown}`
  }  

  arrows(str) {
    return `${icon.arrowDown} ${str} ${icon.arrowDown}`
  }

  start(str) {
    return `${icon.start} ${str}`
  }

  finish(str) {
    return `${icon.finish} ${str}`
  }

  stars(str) {
    return `${icon.starGlowing} ${str} ${icon.starGlowing}`
  }

  circles(str) {
    const arr = []
    for(let prop in icon) {
      if(prop.includes("circle")) arr.push(icon[prop])
    }
    const setIcon = this.randomIcon(arr)

    return `${setIcon} ${str} ${setIcon}`
  }  

  bells(str) {
    return `${icon.bell.repeat(3)}${str}${icon.bell.repeat(3)}`
  }

  exclamations(str) {
    return `${icon.exclamation}${str}${icon.exclamation}`
  }

  search() {
    return `${icon.search}${icon.faceConfounded}`
  }

  ok() {
    return `${icon.handRaised}${icon.faceSmiling}${icon.handOk}`
  }

  hi() {
    return `${icon.handWaving}${icon.faceHeart}`
  }

  sweat() {
    return `${icon.handDown}${icon.faceSweat}`
  }

  worried() {
    return `${icon.handFist}${icon.faceWorried}`
  }


  wait() {
    return `${icon.snail} <b>x ${Math.ceil(Math.random() * 3)}</b>`
  }

  date(str) {
    const arr = str.split("T")

    return `${icon.calendar} ${arr[0].split("-").reverse().join(".")} ${icon.clock} ${arr[1]}`
  }

  /* <========================================== Bot methods =============================================>*/
  parseResult(res, text, reply_markup) {
    const obj = {
      chat_id: res.chat.id,
      message_id: res.message_id,
      message: { text, reply_markup },
      history: [res.message_id]
    }
    if (res.location) {
      obj.location = res.location
      obj.message.text = res.location
    }

    log.save(`{chat_id: ${res.chat.id}, message_id: ${res.message_id}, text: ${JSON.stringify(text)}, reply_markup: ${JSON.stringify(reply_markup)}}`)
    return obj
  }

  editOrSend(bot, chat_id, text, reply_markup, message_id) {
    if(message_id) {
      if(typeof text === "string") {
        return  bot.editMessageText(text, { chat_id, message_id, parse_mode:'HTML', reply_markup })
          .then(res =>  this.parseResult(res, text, reply_markup))
          .catch(e => console.log(e.message))
      }
      else {
        return bot.editMessageReplyMarkup(reply_markup, { parse_mode:'HTML', chat_id, message_id })
          .then(res =>  this.parseResult(res, text, reply_markup))
          .catch(e => console.log(e.message))
      }      
    }
    else {
      if(typeof text === "string") {
        return bot.sendMessage(chat_id, text, { parse_mode:'HTML', reply_markup })
          .then(res =>  this.parseResult(res, text, reply_markup))
          .catch(e => console.log(e.message))
      }
      else {
        const {latitude, longitude} = text

        return bot.sendLocation(chat_id, latitude, longitude, { parse_mode:'HTML', reply_markup })
          .then(res =>  this.parseResult(res, text, reply_markup))
          .catch(e => console.log(e.message))
      }      
    }
  }

  updateUserMessage(bot, chat_id, message) {
    const {history} = message.user
    const {text, reply_markup} = message.user.message

    history.forEach(message_id => bot.deleteMessage(chat_id, message_id))

    return this.editOrSend(bot, chat_id, text, reply_markup)
  }

  updateDriverMessage(bot, chat_id, message) {
    const driverMsg = message.drivers.find(driver => driver.chat_id == chat_id)

    const {history} = driverMsg
    const {text, reply_markup} = driverMsg.message

    history.forEach(message_id => bot.deleteMessage(chat_id, message_id))

    return this.editOrSend(bot, chat_id, text, reply_markup)   
  }

  clearHistory(bot, chat_id, historyArr) {
    return historyArr.forEach(message_id => bot.deleteMessage(chat_id, message_id))
  }

  back(bot, chat_id, message, message_id) {
    const {text, reply_markup} = message.user.message

    return this.editOrSend(bot, chat_id, text, reply_markup, message_id || false)
  }

  /* <======================================== Keyboard methods ===========================================>*/

  // Keyboard
  conFirmButton(request, lang) {
    const textObj = {
      ru: 'Подтвердить',
      ua: 'Пiдтвердити',
      en: 'Confirm'
    }
    return { 
      keyboard: [
        [{ text:textObj[lang], [request]: true }]
      ],
      one_time_keyboard: true,
      resize_keyboard: true
    }
  }

  // Inline Keyboards
  button(prop, lang, shortMode) {    
    const parseProp = prop.split(':')[0]
    const command = this.strToCharCode(parseProp)
    // console.log(`${prop}: ${buttons[parseProp]["label"][lang]} ${buttons[parseProp]["icon"]}`)

    return {
      text: shortMode ? `${buttons[parseProp]["icon"]}` 
        : `${buttons[parseProp]["label"][lang]} ${buttons[parseProp]["icon"]}`,
      callback_data: commands[prop] || commands[parseProp] + prop.slice(parseProp.length)
    }
  }

  inlineButtons(arr, lang, shortMode) {
    const buttonArr = arr.map(buttonName => shortMode ? this.button(buttonName, lang, shortMode)  
      : [this.button(buttonName, lang, shortMode)])

    return { inline_keyboard: shortMode ? [[...buttonArr]] : [...buttonArr] }
  }
  
  inlineButtons2_1(arr, lang, shortMode) {
    const buttonArr = []

    for(let i = 2; i < arr.length; i += 3) {
      buttonArr.push([
        this.button(arr[i - 2], lang, shortMode),
        this.button(arr[i - 1], lang, shortMode)
      ],
      [this.button(arr[i], lang)])
    }
    if(arr % 3) {
      for(let i = arr.length - (arr % 3) - 1; i < arr.length; i += 1) {
        buttonArr.push([this.button(arr[i], lang)])
      }
    }

    return { inline_keyboard: buttonArr }
  }

  inlineFakeBtns(fakeObj, lang, shortMode) {
    const buttonArr = []

    Object.entries(fakeObj).forEach(([key, val]) => {
      buttonArr.push([{text: val[lang], callback_data: 'fake'}])
      buttonArr.push([
        this.button('prev:settings:' + key + ':' + key, lang, shortMode), 
        this.button('next:settings:' + key + ':' + key, lang, shortMode)
      ])
    })
    buttonArr.push([this.button('toMenu', lang, shortMode)])
    return {inline_keyboard: buttonArr}
  }


  /* <======================================== Message methods ===========================================>*/
  // Common messages
  history ([bot, chat_id, message_id], [requests, condition, prev, next, lang, shortMode, logoOffset]) {
    const tail = this.updateCommand([condition, prev, next])
    const textObj = {
      ru:`История поездок:`,
      ua:`Історія поїздок:`,
      en:`Trips history:`,
      noHistory: {
        ru:`Вы еще не совершили поездки!`,
        ua:`Ви ще не зробили поїздки!`,
        en:`You haven't made a trip yet!`,   
      }
    }
    const setLang = this.setLang(lang)
    const text = this.logoOffset(logoOffset) +  (requests ? `<b>${this.circles(textObj[setLang])}</b>\n\n` +  requests
      .filter(({complete_time}) => complete_time)
      .filter((_, idx) => idx >= next && idx < prev)
      .map(request => {
        const {pickup_address, destinations, complete_time, drivers} = request
        const {building, street} = pickup_address[setLang]
        let message = this.date(complete_time) + "\n" 
          + `<b>${this.start(street + (building ? ", " + building : ""))}</b>\n`

        destinations.forEach(destination => {
          const {street, building} = destination[setLang]
          message += `<b>${this.finish(street + (building ? ", " + building : ""))}</b>\n`
        })
        message += (drivers[0] ? this.cars(drivers[0].car_model) : "") + "\n\n"

        return message
      }).join("") :`<b>${textObj.noHistory[setLang]}</b>\n`)
    const buttonArr = ["prev" + tail, "next" + tail, "toMenu"]
    const reply_markup = this.inlineButtons2_1(buttonArr, setLang, shortMode)

    return this.editOrSend(bot, chat_id, text, reply_markup, message_id || false)
  }

  settings ([bot, chat_id, message_id], [lang, shortMode, driver_chat_id, logoOffset]) {
    const btnObj = {
      ru: 'ua',
      ua: 'en',
      en: 'ru'
    }
    const textObj = {
      ru:"<b>Настройки пользователя</b>",
      ua:"<b>Налаштування користувача</b>",
      en:"<b>User settings</b>"
    }
    const setLang = this.setLang(lang)
    const text = this.logoOffset(logoOffset) + textObj[setLang] + " " + this.sweat()
    const buttonArr = [btnObj[setLang], shortMode ? "max" : "min"]
    
    if(driver_chat_id) buttonArr.push("edit:/d" + driver_chat_id)
    buttonArr.push("toMenu")
    const reply_markup = this.inlineButtons(buttonArr, setLang, shortMode) 

    return this.editOrSend(bot, chat_id, text, reply_markup, message_id || false)
  }

  location(bot, chat_id, lang, logoOffset) {
    const textObj = {
      ru:`Подтвердите местоположение`,
      ua:`Підтвердіть місцезнаходження`,
      en:`Confirm location`
    }
    const setLang = this.setLang(lang)
    const text = this.logoOffset(logoOffset) + this.arrows(textObj[setLang])
    const reply_markup = this.conFirmButton("request_location", setLang) 

    return this.editOrSend(bot, chat_id, text, reply_markup, false)
  }

  contact(bot, chat_id, lang, logoOffset) {
    const textObj = {
      ru:"Подтвердить номер телефона:",
      ua:"Підтвердити номер телефону:",
      en:"Confirm phone number:"
    }
    const setLang = this.setLang(lang)
    const text = this.logoOffset(logoOffset) + this.arrows(textObj[setLang])
    const reply_markup = this.conFirmButton("request_contact", setLang) 

    return this.editOrSend(bot, chat_id, text, reply_markup, false)
  }
  
  tripCompleted (bot, chat_id, lang, logoOffset) {
    const textObj = {
      ru:"Заявка завершена! Вы великолепны.",
      ua:"Заявка завершена! Ви прекрасні.",
      en:"Request completed! You are gorgeous."
    }
    const setLang = this.setLang(lang)
    const text = this.logoOffset(logoOffset) + textObj[setLang] + this.ok()

    return this.editOrSend(bot, chat_id, text, {}, false)
  }

  // need to done
  async connect([bot, chat_id, message_id], [phoneOne, phoneTwo, lang, shortMode, isDriver, logoOffset]) {    
    const textObj = {
      ru:"Ожидайте, пытаемся связаться...\n Время ожидания: ",
      ua:"Очикуйте, намагаємося зв'язатися...\n Час очiкування: ",
      en:"Wait, try to contact...\n Wait time: ",
      err: {
        ru:"Сбой соединения...",
        ua:"Збiй з'єднання...",
        en:"Connection failed..."
      }
    }
    const setLang = this.setLang(lang)
    const text = this.logoOffset(logoOffset) + textObj[setLang]
    const buttonArr = ['transfer']
    const reply_markup = isDriver ? 
      { inline_keyboard: [
        [{text: `${phoneOne}<=>${phoneTwo}`, callback_data: 'fake'}],
        [
          this.button("contact", setLang, shortMode),
          this.button("arrived", setLang, shortMode) 
        ],
        [
         // this.button("transfer", setLang, shortMode),
          this.button("complete", setLang, shortMode)
        ],
      ]}
      : this.inlineButtons(buttonArr, setLang, shortMode)
    
    asterisk.sendFile(phoneOne, phoneTwo)

    return this.editOrSend(bot, chat_id, isDriver ? {} : text, reply_markup, message_id || false)
  }

  edit ([bot, chat_id, message_id], [editObj, lang, shortMode, logoOffset]) {
    const textObj = {
      ru:"Вы редактируете:\n{editText}\n\nВыберите пункт для редактирования:",
      ua:"Ви редагуєте:\n{editText}\n\nОберіть пункт для редагування:",
      en:"You edit:\n{editText}\n\nSelect edit item:"
    }
    const defineEditObj = (editObj, lang) => {
      const editText = {}
      const labelObj = {}
      const buttonArr = []           

      if(editObj.hasOwnProperty('pickup_address')) {
        const request = `/r${editObj.chat_id}_${this.setTimestamp(editObj.create_time)}`

        editText.ru = `Заявку  ${request} от <b>${editObj.name}</b>(/u${editObj.chat_id})`
        editText.ua = `Заявку  ${request} від <b>${editObj.name}</b>(/u${editObj.chat_id})`
        editText.en = `Request  ${request} from <b>${editObj.name}</b>(/u${editObj.chat_id})`
        labelObj.pickup_address = {
          ru: `Место отправления`,
          ua: `Місце відправлення`,
          en:`Pickup address`
        }
        labelObj.destinations = {
          ru: `Место назначения`,
          ua: `Місце призначення`,
          en: `Destination address`
        }

        buttonArr.push(...Object.keys(labelObj)
        .map(prop => [{text: labelObj[prop][lang], callback_data:commands['edit'] + `:/r${request}:${prop}`}]))
      }
      else if(editObj.hasOwnProperty('car_model')) {
        const driver = editObj.chat_id

        editText.ru = `Данные водителя <b>${editObj.name}</b>(/d${driver})`
        editText.ua = `Дані водія <b>${editObj.name}</b>(/d${driver})`
        editText.en = `Driver data <b>${editObj.name}</b>(/d${driver})`
        labelObj.car_model = {
          ru: `Модель авто`,
          ua: `Модель авто`,
          en: `Car model`,
        }
        labelObj.car_color = {
          ru: `Цвет авто`,
          ua: `Колір авто`,
          en: `Car color`,
        }
        labelObj.car_number = {
          ru: `Номер авто`,
          ua: `Номер авто`,
          en: `Car number`,
        }

        buttonArr.push(...Object.keys(labelObj)
        .map(prop => [{text: labelObj[prop][lang], callback_data:commands['edit'] + `:/d${driver}:${prop}`}]))
      }
      else return false      
      buttonArr.push([this.button("toMenu", lang, false)])

      return [editText, buttonArr]
    }
  const setLang = this.setLang(lang)

  if(defineEditObj(editObj, setLang)){
    const [editText, buttonArr] = defineEditObj(editObj, setLang)    
    const text = this.logoOffset(logoOffset) + textObj[setLang].replace(/{editText}/, editText[setLang])
    const reply_markup = {inline_keyboard: buttonArr}

    return this.editOrSend(bot, chat_id, text, reply_markup, message_id || false)}
  }

  editProp ([bot, chat_id, message_id], [condition, editProp, lang, shortMode, logoOffset, isNew = false]) {
    const textObj = {
      car_model: {
        ru: `<b>Наберите модель автомобиля:</b>`,
        ua: `<b>Наберіть модель автомобіля:</b>`,
        en: `<b>Enter your car model:</b>`
      },
      car_color: {
        ru: `<b>Наберите цвет автомобиля:</b>`,
        ua: `<b>Наберіть колір автомобіля:</b>`,
        en: `<b>Enter your car color:</b>`
      },
      car_number: {
        ru: `<b>Наберите номер автомобиля:</b>`,
        ua: `<b>Наберіть номер автомобіля:</b>`,
        en: `<b>Enter your car number:</b>`
      },
      pickup_address: {
        ru: `<b>Наберите место отправления:</b>`,
        ua: `<b>Наберіть місце відправлення:</b>`,
        en: `<b>Enter pickup address:</b>`
      },
      destinations: {
        ru: `<b>Наберите место назначения:</b>`,
        ua: `<b>Наберіть місце призначення:</b>`,
        en: `<b>Enter destination address:</b>`
      },
      destinations: {
        ru: `<b>Место назначения:</b>`,
        ua: `<b>Місце призначення:</b>`,
        en: `<b>Destination address:</b>`
      },
      uuid: {
        ru: `Вам отправлено <b>SMS</b> с кодом, после получения <b>SMS</b>, наберите код:`,
        ua: `Вам відправлено <b>SMS</b> з кодом, після отримання <b>SMS</b>, наберіть код:`,
        en: `You have been sent an <b>SMS</b> with a code, after receiving an <b>SMS</b>, enter the code:`
      }
    }
    const labelObj = {
      ru: 'Отменить',
      ua: 'Відмінити',
      en: 'Cansel'
    }    
    const setLang = this.setLang(lang) 
    const text = this.logoOffset(logoOffset) + textObj[editProp][setLang]
    const reply_markup = !isNew ? {inline_keyboard:[[{text:labelObj[setLang], callback_data: commands["edit"] + ":" + condition}]]} : {}

    return this.editOrSend(bot, chat_id, text, reply_markup, message_id || false)
  }

  // User messages
  userMenu ([bot, chat_id, message_id], [user, lang, shortMode, logoOffset]) {
    const textObj = {
      // ru:`Здравствуйте, <b>{name}</b>\nСовершено поездок: <b>{completeCount}</b>\nНачислено бонусов: <b>{bonus}</b>\n\n<b>Выберите пункт меню:</b>`,
      // ua:`Вiтаємо, <b>{name}</b>\nЗдійснено поїздок: <b>{completeCount}</b>\nНараховано бонусів: <b>{bonus}</b>\n\n<b>Оберіть пункт меню:</b>`,
      // en:`Hello, <b>{name}</b>\nTrips made: <b>{completeCount}</b>\nBonuses accrued: <b>{bonus}</b>\n\n<b>Select menu item:</b>`
      ru:`Здравствуйте, <b>{name}</b>\nСовершено поездок: <b>{completeCount}</b>\n\n<b>Выберите пункт меню:</b>`,
      ua:`Вiтаємо, <b>{name}</b>\nЗдійснено поїздок: <b>{completeCount}</b>\n\n<b>Оберіть пункт меню:</b>`,
      en:`Hello, <b>{name}</b>\nTrips made: <b>{completeCount}</b>\n\n<b>Select menu item:</b>`
    }
    const setLang = this.setLang(lang)
    const {name, completeCount, bonus} = user
    const text = this.logoOffset(logoOffset) + this.hi() +  textObj[setLang]
      .replace(/{name}/, this.crowns(name)).replace(/{completeCount}/, this.hearts(completeCount))
      .replace(/{bonus}/, this.money(bonus))
    const buttonArr = ["taxi", "history", "settings"]
    const reply_markup = this.inlineButtons(buttonArr, setLang, shortMode) 

    return this.editOrSend(bot, chat_id, text, reply_markup, message_id || false)
  }

  waitDriver ([bot, chat_id, message_id], [request, lang, shortMode, logoOffset]) {
    const textObj = {
      ru: `Место отправления:\n{address}<b>Ожидайте, идет поиск авто...</b>\n(поиск может занять пару минут)`,
      ua: `Місце відправлення:\n{address}<b>Очикуйте, йде пошук авто...</b>\n(пошук може зайняти пару хвилин)`,
      en: `Starting point:\n{address}<b>Wait, searching for a car...</b>\n(search may take a couple of minutes)`
    }
    const setLang = this.setLang(lang)
    const {pickup_address} = request
    const {building, street} = pickup_address[setLang]
    const address = `<b>${street}${building ? ", " + building : ""}</b>`
    const text = this.logoOffset(logoOffset) + textObj[setLang].replace(/{address}/, this.start(address) + `\n\n${this.search()}`)
    const buttonArr = ["cansel"]
    const reply_markup = this.inlineButtons(buttonArr, setLang, shortMode) 

    return this.editOrSend(bot, chat_id, text, reply_markup, message_id || false)
  }

  noDrivers ([bot, chat_id, message_id], [lang, shortMode, logoOffset]) {
    const textObj =  {
      ru: `<b>Нет водителей...</b>`,
      ua: `<b>Немає водіїв...</b>`,
      en: `<b>No drivers...</b>`
    }
    const setLang = this.setLang(lang)
    const text = this.logoOffset(logoOffset) + this.worried() + textObj[setLang]
    const buttonArr = ["toMenu"]
    const reply_markup = this.inlineButtons(buttonArr, setLang, shortMode) 
      
    return this.editOrSend(bot, chat_id, text, reply_markup, message_id || false)
  }

  yourCar ([bot, chat_id, message_id], [request, driver, lang, shortMode, logoOffset]) {
    const textObj = {
      ru:`Место отправления:\n{address}\nВремя ожидания: {time}\n\nВаш водитель: <b>{name}</b>\nМодель: <b>{car_model}</b>\nЦвет: <b>{car_color}</b>\nНомер: <b>{car_number}</b>`,
      ua:`Місце відправлення:\n{address}\nЧас очiкування:  {time}\n\nВаш водій: <b>{name}</b>\nМодель: <b>{car_model}</b>\nКолір: <b>{car_color}</b>\nНомер: <b>{car_number}</b>`,
      en:`Starting point:\n{address}\nWait time:  {time}\n\nYour driver: <b>{name}</b>\nModel: <b>{car_model}</b>\nColor: <b>{car_color}</b>\nNumber: <b>{car_number}</b>`,
      arrived: {
        ru:`<b>Водитель прибыл</b>`,
        ua:`<b>Водій прибув</b>`,
        en:`<b>Driver arrived</b>`,
      }
    }
    const setLang = this.setLang(lang)
    const {pickup_address} = request
    const {building, street} = pickup_address[setLang]
    const {name, car_model, car_color, car_number} = driver
    const address = `<b>${street}${building ? ", " + building : ""}</b>`
    const text = this.logoOffset(logoOffset) + (textObj[setLang]
      .replace(/{address}/, this.start(address)).replace(/{name}/, this.stars(name)).replace(/{car_model}/, car_model)
      .replace(/{car_color}/, car_color).replace(/{car_number}/, this.exclamations(car_number)))
      .replace(/{time}/, this.wait()) + "\n" + this.carRidesTo(logoOffset)
    const buttonArr = ["contact", "cansel"]
    const reply_markup = this.inlineButtons(buttonArr, setLang, shortMode) 

    return this.editOrSend(bot, chat_id, text, reply_markup, message_id || false)
  }

  arrived ([bot, chat_id, message_id], [request, driver, lang, shortMode, logoOffset]) {
    const textObj = {
      ru:`Место отправления:\n{address}\n\nВаш водитель: <b>{name}</b>\nМодель: <b>{car_model}</b>\nЦвет: <b>{car_color}</b>\nНомер: <b>{car_number}</b>`,
      ua:`Місце відправлення:\n{address}\n\nВаш водій: <b>{name}</b>\nМодель: <b>{car_model}</b>\nКолір: <b>{car_color}</b>\nНомер: <b>{car_number}</b>`,
      en:`Starting point:\n{address}\n\nYour driver: <b>{name}</b>\nModel: <b>{car_model}</b>\nColor: <b>{car_color}</b>\nNumber: <b>{car_number}</b>`,
      arrived: {
        ru:`<b>Водитель прибыл</b>`,
        ua:`<b>Водій прибув</b>`,
        en:`<b>Driver arrived</b>`,
      }
    }
    const setLang = this.setLang(lang)
    const {pickup_address} = request
    const {building, street} = pickup_address[setLang]
    const {name, car_model, car_color, car_number} = driver
    const address = `<b>${street}${building ? ", " + building : ""}</b>`
    const text = this.logoOffset(logoOffset) + textObj[setLang]
      .replace(/{address}/, address).replace(/{name}/, this.stars(name)).replace(/{car_model}/, car_model)
      .replace(/{car_color}/, car_color).replace(/{car_number}/, this.exclamations(car_number)) 
      + `\n\n${this.bells(textObj.arrived[setLang])}`
      + `\n${this.carRidesTo(1)}`
    const buttonArr = ["contact", "cansel"]
    const reply_markup = this.inlineButtons(buttonArr, setLang, shortMode) 

    return this.editOrSend(bot, chat_id, text, reply_markup, message_id || false)
  }

  rate ([bot, chat_id, message_id], [request, lang, shortMode, logoOffset]) {
    const textObj =  {
      ru:`Поездка завершена.\n\n<b>Спасибо за поездку!</b>`,
      ua:"Поїздка закінчена.\n\n<b>Дякуємо за поїздку!</b>",
      en:"Trip completed.\n\n<b>Thanks for the trip!</b>"
    } 
    const setLang = this.setLang(lang)
    const {drivers} = request
    const text = this.logoOffset(logoOffset) + this.ok() + textObj[setLang]
    const reply_markup =  drivers[0] ? { 
      inline_keyboard: [
        [
          this.button("rateDown:" + drivers[0].chat_id, setLang, shortMode), 
          this.button("rateUp:" + drivers[0].chat_id, setLang, shortMode)
        ],
        // [this.button("remember:" + drivers[0].chat_id, setLang, shortMode)]
      ]
    } : {
      inline_keyboard: [
        [this.button("toMenu", setLang, shortMode)]
      ]
    }
    
    return this.editOrSend(bot, chat_id, text, reply_markup, message_id || false)
  }

  // Driver messages
  driverMenu ([bot, chat_id, message_id], [driver, today, yesterday, onLunch, lang, shortMode, logoOffset]) {
    const textObj = {
      ru:"Здравствуйте, <b>{name}</b>\nТекущий баланс: <b>{balance}</b>\nВыполнено заявок\nСегодня: <b>{today}</b>\nВчера: <b>{yesterday}</b>\n\n<b>Ожидаем заявки...</b>",
      ua:"Вітаємо, <b>{name}</b>\nПоточний баланс: <b>{balance}</b>\nВиконано заявок\nСьогоднi: <b>{today}</b>\nУчора: <b>{yesterday}</b>\n\n<b>Очикуємо заявки...</b>",
      en:"Hello, <b>{name}</b>\nCurrent balance: <b>{balance}</b>\nRequests done\nToday: <b>{today}</b>\nYesterday: <b>{yesterday}</b>\n\n<b>Wait request...</b>",    
      onDinner: {
        ru:"Здравствуйте, <b>{name}</b>\nТекущий баланс: <b>{balance}</b>\nВыполнено заявок\nСегодня: <b>{today}</b>\nВчера: <b>{yesterday}</b>\n\n<b>Вы на перерыве, приятного отдыха!</b>",
        ua:"Вітаємо, <b>{name}</b>\nПоточний баланс: <b>{balance}</b>\nВиконано заявок\nСьогоднi: <b>{today}</b>\nУчора: <b>{yesterday}</b>\n\n<b>Вы на перерві, приємного відпочинку!</b>",
        en:"Hello, <b>{name}</b>\nCurrent balance: <b>{balance}</b>\nRequests done\nToday: <b>{today}</b>\nYesterday: <b>{yesterday}</b>\n\n<b>You take a break, enjoy your stay!</b>",      
      }
    }
    const setLang = this.setLang(lang)
    const {name, balance}  = driver
    const text = this.logoOffset(logoOffset) + (onLunch ? textObj.onDinner[setLang].replace(/{yesterday}/, this.hearts(yesterday)).replace(/{name}/, this.crowns(name)).replace(/{balance}/, this.money(balance)).replace(/{today}/, this.hearts(today))
     : textObj[setLang].replace(/{yesterday}/, this.hearts(yesterday)).replace(/{name}/, this.crowns(name)).replace(/{balance}/, this.money(balance)).replace(/{today}/, this.hearts(today)))
    const buttonArr = [onLunch ? "work" : "dinner", "history", "settings"]
    const reply_markup = this.inlineButtons(buttonArr, setLang, shortMode) 

    return this.editOrSend(bot, chat_id, text, reply_markup, message_id || false)
  }

  queueStreetLoc ([bot, chat_id, message_id], [request, lang, shortMode, logoOffset]) {
    const textObj =  {
      ru: `Местоположение клиента:\n{address}`,
      ua: `Місцезнаходження клієнта:\n{address}`,
      en: `Client location:\n{address}`
    }
    const setLang = this.setLang(lang)
    const {pickup_address} = request
    const {building, street} = pickup_address[setLang]
    // const address = `<b>${street}${building ? ", " + building : ""}</b>`    
    // const text = this.logoOffset(logoOffset) + textObj[setLang].replace(/{address}/, address)   
    // const buttonArr = ["take"]
    // const reply_markup = this.inlineButtons(buttonArr, setLang, shortMode) 

    const address = `${street}${building ? ", " + building : ""}`.toUpperCase()
    const text = this.formatLoc(pickup_address[setLang])
    const reply_markup = { inline_keyboard: [
        [{text: address, callback_data: 'fake'}],
        [ this.button("take", setLang, shortMode)]
      ]
    }

    return this.editOrSend(bot, chat_id, text, reply_markup, message_id || false)
  } 

  streetLoc([bot, chat_id, message_id], [request, lang, shortMode, logoOffset]) {
    const textObj =  {
      ru: `Местоположение клиента:\n{address}`,
      ua: `Місцезнаходження клієнта:\n{address}`,
      en: `Client location:\n{address}`
    }
    const setLang = this.setLang(lang)
    const {pickup_address} = request
    const {building, street} = pickup_address[setLang]
    // const address = `<b>${street}${building ? ", " + building : ""}</b>`
    // const text = this.logoOffset(logoOffset) + textObj[setLang].replace(/{address}/, address) 
    // const buttonArr = ['mapLoc', "transfer", "arrived", "contact", "complete"]
    // const reply_markup = { inline_keyboard: [
    //   [
    //     this.button("transfer", setLang, shortMode), 
    //     this.button("mapLoc", setLang, shortMode)
    //   ],
    //   [
    //     this.button("contact", setLang, shortMode),
    //     this.button("arrived", setLang, shortMode) 
    //   ],
    //   [ 
    //     this.button("complete", setLang, shortMode)
    //   ]
    //   ]
    // }

    const address = `${street}${building ? ", " + building : ""}`.toUpperCase()
    const text = this.formatLoc(pickup_address[setLang])
    const reply_markup = { inline_keyboard: [
      [{text: address, callback_data: 'fake'}],
      [
        this.button("contact", setLang, shortMode),
        this.button("arrived", setLang, shortMode) 
      ],
      [
        // this.button("transfer", setLang, shortMode),
        this.button("complete", setLang, shortMode)
      ],
      ]
    }

    return this.editOrSend(bot, chat_id, text, reply_markup, message_id || false)
  }

  //  need to create
  canselled([bot, chat_id], [lang, shortMode, logoOffset]) {
    const textObj =  {
      ru: `Клиент отменил заявку`,
      ua: `Клієнт скасував заявку`,
      en: `Client canselled request`
    }
    const setLang = this.setLang(lang)
    const text = this.logoOffset(logoOffset) + textObj[setLang] + this.worried()
    const reply_markup = { inline_keyboard: [[ this.button("hide", setLang, shortMode)]] }
    
    return this.editOrSend(bot, chat_id, text, reply_markup)
  }

  getted([bot, chat_id, message_id], [lang, shortMode, logoOffset]) {
    const textObj =  {
      ru: `Упс, заявку уже забрали`,
      ua: `Упс, заявку вже забрали`,
      en: `Oups, request has been taken`
    }
    const setLang = this.setLang(lang)
    const text = this.logoOffset(logoOffset) + textObj[setLang] + this.worried()
    const reply_markup = { inline_keyboard: [[ this.button("hide", setLang, shortMode)]] }

    return this.editOrSend(bot, chat_id, text, reply_markup, message_id || false)
  }

  mapLoc ([bot, chat_id, message_id], [request, lang, shortMode]) {
    const setLang = this.setLang(lang)
    const {pickup_address} = request
    const text = this.formatLoc(pickup_address[setLang])
    const buttonArr = ["hide"]
    const reply_markup = this.inlineButtons(buttonArr, setLang, shortMode) 

    return this.editOrSend(bot, chat_id, text, reply_markup, message_id || false)
  }

  // Admin messages
  adminMenu ([bot, chat_id, message_id], [users, lang, shortMode, logoOffset]) {
    const textObj = {
      ru:"Администраторы онлайн:\n{admins}\nВыберите пункт меню:",
      ua:"Адміністратори онлайн:\n{admins}\nОберіть пункт меню:",
      en:"Admins online:\n{admins}\nSelect menu item:"
    }
    const setLang = this.setLang(lang)
    const admins = users.filter(({type}) => type == 'admin')
      .map(({name, chat_id}) => `<b>${name}</b>(/u${chat_id})`).join("\n") + "\n"
    const text = this.logoOffset(logoOffset) + textObj[setLang].replace(/{admins}/, admins)
    const buttonArr = ["users", "drivers", "requests", "botSettings", "logout"]
    const reply_markup = this.inlineButtons(buttonArr, setLang, shortMode) 
    
    return this.editOrSend(bot, chat_id, text, reply_markup, message_id || false)
  }

  users ([bot, chat_id, message_id], [users, condition, prev, next, lang, shortMode, logoOffset]) {    
    const textObj = {
      ru:`Список пользователей:\n{userList}`,
      ua:`Список користувачiв:\n{userList}`,
      en:`Users list:\n{userList}`
    } 
    const setLang = this.setLang(lang)
    const tail = this.updateCommand([condition, prev, next])
    const userList = users.filter((_, idx) => idx >= next && idx < prev)
      .map(({name, chat_id}) => `<b>${name}</b>(/u${chat_id})`).join("\n") + "\n"
    const text = this.logoOffset(logoOffset) + textObj[setLang].replace(/{userList}/, userList)
    const buttonArr = ["prev" + tail, "next" + tail, "toMenu"]
    const reply_markup =  this.inlineButtons2_1(buttonArr, setLang, shortMode) 

    return this.editOrSend(bot, chat_id, text, reply_markup, message_id || false)
  }

  drivers ([bot, chat_id, message_id], [drivers, condition, prev, next, lang, shortMode, logoOffset]) {
    const textObj = {
      ru:`Список водителей:\n{driverList}`,
      ua:`Список водіїв:\n{driverList}`,
      en:`Drivers list:\n{driverList}`
    }
    const setLang = this.setLang(lang)
    const tail = this.updateCommand([condition, prev, next])
    const driverList = drivers.filter((_, idx) => idx >= next && idx < prev)
      .map(({name, chat_id}) => `<b>${name}</b>(/d${chat_id})`).join("\n") + "\n"
    const text = this.logoOffset(logoOffset) + textObj[setLang].replace(/{driverList}/, driverList)
    const buttonArr = ["prev" + tail, "next" + tail, "toMenu"]
    const reply_markup =  this.inlineButtons2_1(buttonArr, setLang, shortMode) 

    return this.editOrSend(bot, chat_id, text, reply_markup, message_id || false)
  }

  requests ([bot, chat_id, message_id], [requests, condition, prev, next, lang, shortMode, logoOffset]) {
    const textObj = {
      ru:`Список заявок:\n{requestList}`,
      ua:`Список заявок:\n{requestList}`,
      en:`Requests list:\n{requestList}`
    }
    const setLang = this.setLang(lang)
    const tail = this.updateCommand([condition, prev, next])
    const requestList = requests.filter((_, idx) => idx >= next && idx < prev)
      .map(({pickup_address, create_time, chat_id}) => {
        const {street, building} = pickup_address[setLang]
        return `<b>${street}${building ? ", " + building : ""}</b>\n/r${chat_id}_${this.setTimestamp(create_time)}\n`
      }).join("\n") + "\n"
    const text = this.logoOffset(logoOffset) + textObj[setLang].replace(/{requestList}/, requestList)
    const buttonArr = ["prev" + tail, "next" + tail, "toMenu"]
    const reply_markup = this.inlineButtons2_1(buttonArr, setLang, shortMode)

    return this.editOrSend(bot, chat_id, text, reply_markup, message_id || false)
  }

  request ([bot, chat_id, message_id], [request, lang, shortMode, logoOffset]) {
    const textObj = {
      ru:`Клиент: {user}\nВодитель: {driver}\nОткуда: <b>{from}</b>\nКуда: <b>{to}</b>\n{time}`,
      ua:`Клієнт: {user}\nВодій: {driver}\nЗвідки: <b>{from}</b>\nКуди: <b>{to}</b>\n{time}`,
      en:`Client: {user}\nDriver: {driver}\nFrom: <b>{from}</b>\nTo: <b>{to}</b>\n{time}`
    }
    const setLang = this.setLang(lang)
    const {
      name, drivers, pickup_address, destinations, pickup_time, complete_time, create_time
    }  = request
    const {street, building} = pickup_address[setLang]
    const user = `${name}(/u${request.chat_id})`
    const driver = `${drivers[0].name}(/d${drivers[0].chat_id})`
    const from = `<b>${street}${building ? ", " + building : ""}</b>`
    const to = destinations.map( destination => {
      const {street, building} = destination

      return  `<b>${street}${building ? ", " + building : ""}</b>`
    }).join('\n')
    const time = `${pickup_time} ${complete_time}`

    const text = this.logoOffset(logoOffset) + textObj[setLang]
      .replace(/{user}/, user).replace(/{driver}/, driver).replace(/{from}/, from)
      .replace(/{to}/, to).replace(/{time}/, time)
    const buttonArr = ["transfer", "edit:/r" + request.chat_id + '_' + this.setTimestamp(create_time), "cansel", "complete", "toMenu"]
    const reply_markup = this.inlineButtons(buttonArr, setLang, shortMode) 

    return this.editOrSend(bot, chat_id, text, reply_markup, message_id || false)
  }
 
  user ([bot, chat_id, message_id], [user, lang, shortMode, logoOffset]) {
    const textObj = {
      ru:`Имя: <b>{name}</b>\nТелефон: <b>{phone}</b>\nОтмен: <b>{canselCount}</b>\nПоездок: <b>{completeCount}</b>`,
      ua:`Ім'я: <b> {name}</b>\nТелефон: <b>{phone}</b>\nВідмін: <b>{canselCount}</b>\nПоїздок: <b>{completeCount}</b>`,
      en:`Name: <b>{name}</b>\nPhone: <b>{phone}</b>\nCansels: <b>{canselCount}</b>\nTrips: <b>{completeCount}</b>`
    } 
    const setLang = this.setLang(lang)
    const { name, phone, canselCount, completeCount, ban} = user
    const text = this.logoOffset(logoOffset) + textObj[setLang]
      .replace(/{name}/, name).replace(/{phone}/, phone)
      .replace(/{canselCount}/, canselCount).replace(/{completeCount}/, completeCount)
    const buttonArr = ["requests:/u" + user.chat_id, ban ? "unban:/u" + user.chat_id : "ban:/u" + user.chat_id, "toMenu"]
    const reply_markup = this.inlineButtons(buttonArr, setLang, shortMode) 

    return this.editOrSend(bot, chat_id, text, reply_markup, message_id || false)
  }

  driver ([bot, chat_id, message_id], [driver, lang, shortMode, logoOffset]) { 
    const textObj = {
      ru:`Имя: <b>{name}</b>\nТелефон: <b>{phone}</b>\nБаланс: <b>{balance}</b>\nМодель: <b>{car_model}</b>\nЦвет: <b>{car_color}</b>\nНомер: <b>{car_number}</b>`,
      ua:`Ім'я: <b> {name}</b>\nТелефон: <b>{phone}</b>\nБаланс: <b>{balance}</b>\nМодель: <b>{car_model}</b>\nКолір: <b>{car_color}</b>\nНомер: <b>{car_number}</b>`,
      en:`Name: <b>{name}</b>\nPhone: <b>{phone}</b>\nBalance: <b>{balance}</b>\nModel: <b>{car_model}</b>\nColor: <b>{car_color}</b>\nNumber:<b>{car_number}</b>`
    } 
    const setLang = this.setLang(lang)  
    const { name, balance, phone, car_model, car_color, car_number, ban } = driver
    const text = this.logoOffset(logoOffset) + textObj[setLang]
      .replace(/{name}/, name).replace(/{balance}/, balance).replace(/{car_number}/, car_number)
      .replace(/{car_model}/, car_model).replace(/{car_color}/, car_color).replace(/{phone}/, phone)
    const buttonArr = ["edit:/d" + driver.chat_id, "requests:/d" + driver.chat_id, ban ? "unban:/d" + driver.chat_id : "ban:/d" + driver.chat_id, "toMenu"]
    const reply_markup = this.inlineButtons(buttonArr, setLang, shortMode) 

    return this.editOrSend(bot, chat_id, text, reply_markup, message_id || false)
  }

  botSettings ([bot, chat_id, message_id], [settings, lang, shortMode, logoOffset]) {
    const textObj = {
      ru:"Настройки бота:",
      ua:"Налаштування бота:",
      en:"Bot settings:"
    }
    const {maxRows, alertDelay, rateDelay, deliveryDelay} = settings
    const fakeButton = {
      deliveryDelay: {
        ru: `Подача: ${deliveryDelay} мин.`,
        ua: `Подача: ${deliveryDelay} хв.`,
        en: `Delivery: ${deliveryDelay} min.`
      },
      rateDelay: {
        ru: `Оценка: ${rateDelay} мин.`,
        ua: `Оцінка: ${rateDelay} хв.`,
        en: `Rate: ${rateDelay} min.`
      },
      alertDelay: {
        ru: `Уведомление: ${alertDelay} сек.`,
        ua: `Сповіщення: ${alertDelay} сек.`,
        en: `Alert: ${alertDelay} sec.`
      },
      maxRows: {
        ru: `Cтолбцов: ${maxRows}`,
        ua: `Cтовбців: ${maxRows}`,
        en: `Rows: ${maxRows}`
      },
    }
    const setLang = this.setLang(lang)
    const text = this.logoOffset(logoOffset) + textObj[setLang]
    const reply_markup = this.inlineFakeBtns(fakeButton, setLang, shortMode) 

    return this.editOrSend(bot, chat_id, text, reply_markup, message_id || false)
  }  
}

module.exports =  {Messages}