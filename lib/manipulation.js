const axios = require('axios')
const encode = require('nodejs-base64-encode')
const baseUrl = "http://127.0.0.1:3000/api"
const { path } = require('../config')
const { Log } = require('./log')
const log = new Log(path + '/log/db')


class Message {
  constructor() {
    this.path = "message"
    this.options = {
      headers: { 'Content-Type': 'application/json' }
    }
  }

  // Data object methods
  createObj(obj) {
    const {chat_id, message_id, message, history = [message_id]} = obj

    return {
      user: { chat_id, message_id, message, history },
      drivers: []
    }
  }

  editObj(obj) {
    const {user, drivers} = obj

    // console.log("edit\n", drivers)

    return {
      user,
      drivers: drivers.map(({ chat_id, message_id, message, history}) => {
        return {chat_id, message_id, message, history}
      })
    }
  }

  // DB methods
  create(obj) { 
    const data = this.createObj(obj)

    axios.post(`${baseUrl}/${this.path}/create`, data, this.options)
      .then(response => log.save(response.data))
      .catch(error => log.save(error))    
  }  

  async edit(message) {
    const data = await this.editObj(message)

    axios.post(`${baseUrl}/${this.path}/edit`, data, this.options)
      .then(response => log.save(response.data))
      .catch(error => log.save(error))
  }

  find(message_id) {
    const query = message_id ? `?message_id=${message_id}` : ``

    return axios.get(`${baseUrl}/${this.path}/${query}`)
      .then(response => response.data)
      .catch(error => log.save(error))
  }

  findUserMsg(chat_id) {
    const query = chat_id ? `?chat_id=${chat_id}` : ``

    return axios.get(`${baseUrl}/${this.path}/user/${query}`)
      .then(response => response.data)
      .catch(error => log.save(error))
  }

  findDriverMsgs(chat_id) {
    const query = chat_id ? `?chat_id=${chat_id}` : ``

    return axios.get(`${baseUrl}/${this.path}/driver/${query}`)
      .then(response => response.data)
      .catch(error => log.save(error))
  }

  getMessages(message_id) {
    return this.find(message_id)
      .then(message => {
        if (message) return [
          [message.user.chat_id, message.user.message_id], 
          ...message.drivers.map(el => [el.chat_id, el.message_id])
        ]
      })
      .catch(error => log.save(error))
  }

  delete(chat_id) {
    axios.delete(`${baseUrl}/${this.path}/?chat_id=${chat_id}`)
      .then(response => log.save(response.data))
      .catch(error => log.save(error))
  }
}

class User {
  constructor() {
    this.path = "user"
    this.options = {
      headers: { 'Content-Type': 'application/json' }
    }
  }

  // Data object methods
  createObj(obj) {
    const {chat_id, name, phone, language_code, edit} = obj

    return {
      chat_id, name, phone, language_code, canselCount: 0, completeCount: 0, uuid: '', 
      ban: false, shortMode: false, edit, bonus: 0
    }
  }

  setSMS() {
    return '12345'
  }

  // DB methods
  create(obj) {
    const data = this.createObj(obj)

    axios.post(`${baseUrl}/${this.path}/create`, data, this.options)
      .then(response => log.save(response.data))
      .catch(error => log.save(error))
  }
  
  edit(obj) {
    axios.post(`${baseUrl}/${this.path}/edit`, obj, this.options)
      .then(response => log.save(response.data))
      .catch(error => log.save(error))
  }

  find(chat_id) {
    const query = chat_id ? `?chat_id=${chat_id}` : ``

    return axios.get(`${baseUrl}/${this.path}/${query}`)
      .then(response => response.data)
      .catch(error => log.save(error))
  }

  async canselCountUp(chat_id) {
    const user = await this.find(chat_id)

    this.edit({...user, canselCount: ++user.canselCount })
  }

  async completeCountUp(chat_id) {
    const user = await this.find(chat_id)

    this.edit({...user, completeCount: ++user.completeCount })
  }
}

class Driver {
  constructor() {
    this.path = "driver"
    this.options = {
      headers: { 'Content-Type': 'application/json' }
    }
  }

  // Data object methods
  createObj(obj) {
    const {chat_id, name, phone, language_code, edit} = obj

    return {
      chat_id, name, phone, completed: [], car_model: '', car_color: '', car_number: '', 
      ban: false, shortMode: false, language_code, edit: edit || [], onLunch: false, balance: 0
    }
  }

  // DB methods
  create(obj) {
    const data = this.createObj(obj)

    axios.post(`${baseUrl}/${this.path}/create`, data, this.options)
      .then(response => log.save(response.data))
      .catch(error => log.save(error))
  }

  edit(obj) {
    axios.post(`${baseUrl}/${this.path}/edit`, obj, this.options)
      .then(response => log.save(response.data))
      .catch(error => log.save(error))
  }

  find(chat_id) {
    const query = chat_id ? `?chat_id=${chat_id}` : ``

    return axios.get(`${baseUrl}/${this.path}/${query}`)
      .then(response => response.data)
      .catch(error => log.save(error))
  }

  ligaCreatObj(obj) {
    const {driver_id, call_id, name, phone, car_model, car_color, car_number} = obj

    return { driver_id, call_id, name, phone, car_model, car_color, car_number }
  }


  ligaCreate(obj) {
    const data = this.ligaCreatObj(obj)

    axios.post(`${baseUrl}/${this.path}/liga/create`, data, this.options)
      .then(response => log.save(response.data))
      .catch(error => log.save(error))
  }

  ligaFind(driver_id) {
    const query = driver_id ? `?driver_id=${driver_id}` : ``

    return axios.get(`${baseUrl}/${this.path}/liga/${query}`)
      .then(response => response.data)
      .catch(error => log.save(error))
  }
}

class Request {
  constructor() {
    this.path = "request"
    this.mapUrl = 'https://nominatim.openstreetmap.org'
    this.options = {
      headers: { 'Content-Type': 'application/json' }
    }
  }

  // Data object methods
  setTimestamp(strDate) {    
    return new Date(strDate).valueOf()
  }

  setDate(delay = 0, setTime = false) {
    const date = new Date(Date.now() + delay)
    const updateObj = (obj) => {
      const newObj = {}

      for(let prop in obj) {
        if(obj[prop].toString().length < 2) {
          newObj[prop] = `0${obj[prop]}`
        }
        else {
          newObj[prop] = obj[prop]
        }
      }

      return newObj
    }

    const d = updateObj({
      year: date.getFullYear(),
      month: date.getMonth() + 1,
      day: date.getDate(),
      hours: setTime ? setTime.split(":")[0] : date.getHours(),
      minites: setTime ? setTime.split(":")[1] : date.getMinutes(),
      seconds: setTime ? setTime.split(":")[2] : date.getSeconds()
    })
    
    return `${d.year}-${d.month}-${d.day}T${d.hours}:${d.minites}:${d.seconds}`   
  }

  timestampToString(timestamp) {
    const date = new Date(Number(timestamp))
    const updateObj = (obj) => {
      const newObj = {}

      for(let prop in obj) {
        if(obj[prop].toString().length < 2) {
          newObj[prop] = `0${obj[prop]}`
        }
        else {
          newObj[prop] = obj[prop]
        }
      }

      return newObj
    }

    const d = updateObj({
      year: date.getFullYear(),
      month: date.getMonth() + 1,
      day: date.getDate(),
      hours: date.getHours(),
      minites: date.getMinutes(),
      seconds: date.getSeconds()
    })
    
    return `${d.year}-${d.month}-${d.day}T${d.hours}:${d.minites}:${d.seconds}`   
  }

  setAddress(location) {
    const {latitude, longitude} = location
    const langArr = ['ru', 'ua', 'en']

    return this.findAddress({latitude, longitude})
      .then(address => {
        if(address) return address
        else {
          const addresses = langArr
            .map(lang => axios.get(`${this.mapUrl}/reverse?lat=${latitude}&lon=${longitude}&format=json&accept-language=${lang}`)
            .then(response => {
              const {house_number, road, town, village } = response.data.address

              return { house_number, road, town: town || village, lang }
            }))    

          return Promise.all(addresses)
            .then(addresses => {
              const request_address = {}
              const newAddresses = addresses
                .filter(({house_number, road}) => house_number&&road)
                .map(({ house_number, road, town, lang }) => {
                  return axios.get(encodeURI(`${this.mapUrl}/search?q=${house_number}+${road},+${town}&format=json&polygon_geojson=1&addressdetails=1&accept-language=${lang}`))
                    .then(response => {return {...response.data[0], lang}})
                })
              
              if(newAddresses.length) Promise.all(newAddresses)
                .then(addresses => {
                  const full_display_name = {}, full_address = {}            
                  const obj = addresses.map(address => {
                    full_display_name[address.lang] = address.display_name
                    full_address[address.lang] = address.address

                    return address
                  })[0]
                  if(obj) delete obj.lang

                  return {...obj, display_name: full_display_name, address: full_address}
                })
                .then(address => {
                  if(address) this.createAddress(address)
                })
                .catch(e => log.save(e))

              addresses.forEach(({ house_number, road, town, lang }) => {
                if(road) request_address[lang] = {
                  town, 
                  street: road,
                  building: house_number ? house_number : '',
                  comment:'',
                  lat: latitude,
                  lng: longitude
                }          
              })

              return request_address
            })
            .catch(e => log.save(e))}
            })    
  }

  createObj(obj, delay) {
    const {chat_id, name, location, phone, order_id} = obj
    
    return this.setAddress(location).then(address => { 
      
      return {
        chat_id, 
        name,
        phone,
        state: 'processed',
        drivers: [],
        pickup_address: address,
        pickup_time: this.setDate(delay),
        create_time: this.setDate(),
        complete_time:'',
        order_id: order_id || 0,
        car_type:'6',
        car_extras:[],
        driver_extras:[],
        comment:'Request from telegram',
        destinations:[]
      }
    })    
  }

  createAddressObj(obj) {
    const { boundingbox, lat, lon, display_name, type, importance, geojson, address } = obj 

    return {
      boundingbox,
      lat,
      lon,
      display_name,
      type,
      importance,
      address,
      geojson
    }
  }

  editObj(obj) {
    const {location, state, drivers} = obj 

    delete obj._id
    if(state == 'completed') obj.complete_time = this.setDate();
    if(location) {    

      return this.setAddress(location).then(address => {
        return {
          ...obj,
          destinations:[
            ...obj.destinations,
            address
          ],
          drivers: drivers.map(driver => {
            const { chat_id, message_id, name, phone, car_model,car_color, car_number, rate, language_code, shortMode } = driver

            return { chat_id, message_id, name, phone, car_model,car_color, car_number, rate, language_code, shortMode }
          })
        }
      })
    }
    else return obj 
  }

  // DB methods
  async create(obj, delay) {
    const data = await this.createObj(obj, delay)

    axios.post(`${baseUrl}/${this.path}/create`, data, this.options)
      .then(response => log.save(response.data))
      .catch(error => log.save(error))
  }

  createAddress(obj) {
    const data = this.createAddressObj(obj)

    axios.post(`${baseUrl}/${this.path}/createaddress`, data, this.options)
      .then(response => log.save(response.data))
      .catch(error => log.save(error))
  }

  findAddress({latitude, longitude}) {
    return axios.get(`${baseUrl}/${this.path}/findaddress/?lat=${latitude}&lon=${longitude}`, this.options)
      .then(response => response.data)
      .catch(e => log.save(e))
  }

  async edit(obj) {
    const data = await this.editObj(obj)

    axios.post(`${baseUrl}/${this.path}/edit`, data, this.options)
      .then(response => log.save(response.data))
      .catch(error => log.save(error))
  }

  find(chat_id) {
    const query = chat_id ? `?chat_id=${chat_id}` : ``

    return axios.get(`${baseUrl}/${this.path}/${query}`)
      .then(response => response.data)
      .catch(error => log.save(error))
  }

  findByOrderId(order_id) {
    const query = order_id ? `?order_id=${order_id}` : ``

    return axios.get(`${baseUrl}/${this.path}/${query}`)
      .then(response => response.data)
      .catch(error => log.save(error))
  }

  current(chat_id) {
    return axios.get(`${baseUrl}/${this.path}/current/?chat_id=${chat_id}`)
      .then(response => response.data)
      .catch(error => log.save(error))
  }

  driver(chat_id, setTime = '08:00:00') {
    const query = chat_id ? `?chat_id=${chat_id}` : ``

    return axios.get(`${baseUrl}/${this.path}/driver/${query}`)
      .then(({data}) => {
        const result = []        
        if(data.length) {
          const workTime = {
            start: this.setDate(0, setTime),
            end: this.setDate(24 * 60 * 60 * 1000 - 1, setTime),
            yesterday: {
              start: this.setDate(-24 * 60 * 60 * 1000, setTime),
              end: this.setDate(-1, setTime),
            }
          }

          result.push(data)
          result.push(data.filter(({complete_time}) => {
            return (this.setTimestamp(complete_time) >= this.setTimestamp(workTime.start) 
              && this.setTimestamp(complete_time) <= this.setTimestamp(workTime.end))
          }))
          result.push(data.filter(({complete_time}) => {
            return (this.setTimestamp(complete_time) >= this.setTimestamp(workTime.yesterday.start) 
              && this.setTimestamp(complete_time) <= this.setTimestamp(workTime.yesterday.end))
          }))

          return {all: result[0], today: result[1], yesterday: result[2]}
        }
        else return false
      })
      .catch(error => log.save(error))
  }

  createTime(chat_id, timestamp) {
    return this.find(chat_id)
      .then(requests => requests.find(({create_time}) => create_time == this.timestampToString(timestamp)))
  }

  delete(chat_id) {
    axios.delete(`${baseUrl}/${this.path}/?chat_id=${chat_id}`)
      .then(response => log.save(response.data))
      .catch(error => log.save(error))
  }
}

class Settings {
  constructor() {
    this.path = "settings"
    this.options = {
      headers: { 'Content-Type': 'application/json' }
    }
  }

  // DB methods
  create(obj) {
    return axios.post(`${baseUrl}/${this.path}/create`, obj, this.options)
      .then(response => response.data)
      .catch(error => log.save(error))
  }
  
  edit(obj) {
    axios.post(`${baseUrl}/${this.path}/edit`, obj, this.options)
      .then(response => log.save(response.data))
      .catch(error => log.save(error))
  }

  find() {

    return axios.get(`${baseUrl}/${this.path}/`)
      .then(response => response.data)
      .catch(error => log.save(error))
  }
}

class Liga {
  constructor(companies, locations, lang) {
    this.lang = lang || 'ru'
    this.companies = companies
    this.locations = locations
    this.api_key_name = "pbx_key"
    this.api_key = "b2e93c15c05ad710bcf39ed6f0a5aaec"
    this.client_domain = "api-senat-rubezhn.ligataxi.com"
    this.url = `http://${this.client_domain}/api/v1/client`
    this.urlRPC = `https://${this.client_domain}/rpc`
    this.options = {
      headers: { 'Content-Type': 'application/json', 'api-key': this.api_key }
    }
  }

  companyId (town) {
    return Object.entries(this.companies).filter(([_, val]) => typeof val != 'function' && val.towns.includes(town)).map(([key]) => Number(key))
  }

  localityId (town) {
    return Number(Object.entries(this.locations).find(([_, val]) => town.includes(val))[0])
  }

  typeId (id) {
    return this.companies[id]["type_id"]
  }

  carTypeId (id) {
    return this.companies[id]["car_type_id"]
  }

  createObj(request, phone) {
    const {pickup_address, pickup_time } = request
    const { town, street, building, lat, lng } = pickup_address[this.lang]
    const id = 3 || this.companyId(town)[0]

    return {
      jsonrpc: "2.0",
      method: "order.create",
      params: {
        company_id: id,
        type_id: this.typeId(id),
        phone,
        car_type_id: this.carTypeId(id),
        pickup_time,
        auto_dispatch: true,
        distance: 0,
        pickup_address: {
          locality_id: this.localityId(town),
          street,
          building,
          latlng: {
            lat,
            lng
          }
        }
      },
      id: Date.now()
    }    
  }

  editObj(request) {
    const { pickup_address, pickup_time, order_id } = request
    const { town, street, building, lat, lng } = pickup_address[this.lang]
    const id = 3 || this.companyId(town)[0]

    return {
      jsonrpc: "2.0",
      method: "order.update",
      params: {
        order_id,
        company_id: id,
        type_id: this.typeId(id),
        car_type_id: this.carTypeId(id),
        pickup_time,
        auto_dispatch: true,
        distance: 0,
        pickup_address: {
          locality_id: this.localityId(town),
          street,
          building,
          latlng: {
            lat,
            lng
          }
        }
      },
      driver_id: 131,
      id: Date.now()
    }    
  }

  auth() {
    return {
      headers: {
        ...this.options.headers, 
        Authorization: `Basic ${encode.encode(`${this.api_key_name}:${this.api_key}`, 'base64')}`
      }
    }
  }

  timeout(timeout) {
    return { headers: { 'Content-Type': 'application/json' }, timeout}
  }

  async isUp(timeout) {
    return axios.get(`http://127.0.0.1:3001/isup/`, this.timeout(timeout))
      .then(response => response.data)
      .catch(error => {
        console.log(error.message)
        return false
      })
  }

  driversLocs(company_id) {
    return axios.get(`${this.url}/company/${company_id}/drivers/`, this.options)
      .then(response => response.data)
      .catch(error => console.log(error))
  }

  /* <============================ JSON-RPC =================================> */
  createRequestRPC(request, phone) {
    const data = this.createObj(request, phone)

    return axios.post(`${this.urlRPC}/`, data, this.auth())
      .then(res => res.data.result.order_id)
      .catch(e => log.save(e))
  }

  updateRequestRPC(request) {
    const data = this.editObj(request)

    this.sendRPCreq(data)    
  }

  changeStateRPC(requestId, state) {
    const data = JSON.stringify({
      jsonrpc:"2.0",
      method: "order.state",
      params:{ order_id: requestId, state },
      id:  Date.now()
    })

    this.sendRPCreq(data)    
  }

  canselRequestRPC(requestId, cancel_reason_id) {
    const data = JSON.stringify({
      jsonrpc:"2.0",
      method: "order.cancel",
      params:{ order_id: requestId, cancel_reason_id },
      id:  Date.now()
    })

    return axios.post(`${this.urlRPC}/`, data, this.auth())
      .then(res => res.data.result.order_id)
      .catch(e => log.save(e))    
  }

  restoreRequestRPC(requestId) {
    const data = JSON.stringify({
      jsonrpc:"2.0",
      method: "order.restore",
      params:{ order_id: requestId },
      id:  Date.now()
    })

    return axios.post(`${this.urlRPC}/`, data, this.auth())
      .then(res => res.data.result.order_id)
      .catch(e => log.save(e)) 
  }
}

module.exports = {User, Request, Message, Driver, Settings, Liga}