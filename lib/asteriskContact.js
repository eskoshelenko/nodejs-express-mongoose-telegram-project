const fs = require('fs')
const { path } = require('../config')
const { exec } = require("child_process")
const { Log } = require('./log')
const log = new Log(path + '/log/bot')
const settingsObj = {
  data: `Channel: Local/{phoneOne}@from-internal
CallerID: "Conference"
MaxRetries: 0
RetryTime: 30
WaitTime: 40
Context: conference
Extension: {phoneTwo}
Priority: 1`,
  filePath: path.split("\\").join('/') + `/outgoing`,
  asteriskPath: `/var/spool/asterisk/outgoing`,
  asteriskIP: `<AsteriskIP>`,
  asteriskPWD: `AsteriskPWD`,
  bashCmnd(fileName) {
    return `sshpass -p '${this.asteriskPWD}' scp ${this.filePath}/${fileName} root@${this.asteriskIP}:${this.asteriskPath}`
  }
}
class CallFile {
  constructor(obj) {
   this.obj = obj
  }

  createFile(data, name) {
    fs.appendFile(this.obj.filePath + '/' + name, data, err => {
      if (err) {
        log.save(`File create ERROR: ${err.message}`)
        return false          
      }
    })  
  }

  async sendFile(phoneOne, phoneTwo, userNames = 'DefaultNames') {
    const data = this.obj.data
      .replace(/{phoneOne}/, phoneOne.toString().slice(-10)).replace(/{phoneTwo}/, phoneTwo.toString().slice(-10)).replace(/{userNames}/, userNames)
    const fileName = Date.now() + `.call`
    
    await this.createFile(data, fileName)
    
    exec(this.obj.bashCmnd(fileName), err => {
      if (err) log.save(`File send ERROR: ${err.message}`)
      log.save(`CallTo: { phoneOne: ${phoneOne}, phoneTwo: ${phoneTwo}, reason: CONTACT }`)
    })    
  }
}



module.exports =  new CallFile(settingsObj)