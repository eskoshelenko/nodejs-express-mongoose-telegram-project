const express = require('express')
const app = express()
// import Routes
const ligaRoute = require('./routes/liga')

//Middleware
app.use(express.json())

app.get('/isup', async (req,res) => {
  res.setHeader('content-Type', 'application/json')
  res.send(JSON.stringify(true))
})

// Route Middlewares
app.use('/api/liga', ligaRoute)

app.listen(3001, () => console.log('Liga server Up and Running...'))