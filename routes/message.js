const router = require('express').Router()
const Message = require('../models/Message')

// Create Message
router.post('/create', async (req,res) => {
  const message = new Message(req.body)

  res.setHeader('Content-Type','application/json')
  try {
    const savedMessage = await message.save()

    res.setHeader('content-Type', 'application/json')    
    res.send(`${req.body.user.chat_id} message has been created`)
  } catch (e) {
    res.status(400).send(`${req.body.user.chat_id} message fail to create`)
  }
})

// Edit Message
router.post('/edit', async (req,res) => {  
  await Message.findOneAndUpdate({"user.chat_id":req.body.user.chat_id}, req.body, {
    useFindAndModify: false,
    new: true
  }, (err, doc) => {
    res.setHeader('content-Type', 'application/json')
    if(err) res.send(JSON.stringify(err.message));
    else{
      if(doc) res.send(JSON.stringify(`${req.body.user.chat_id} message has been edited`));
      else res.send(JSON.stringify(`No messages match`));
    }    
  })  
})

// Find Message
router.get('/', async (req,res) => {
  const {message_id} = req.query

  res.setHeader('content-Type', 'application/json')
  if(message_id) await Message.findOne({"user.message_id":message_id})
    .then(async message => {
      if(message) {        
        res.send(JSON.stringify(message))
      } else {
        await Message.findOne({"drivers":{ $elemMatch: {message_id}}})
          .then(async message => {
            if(message) {        
              res.send(JSON.stringify(message))
            } else {
              res.send(JSON.stringify(false))
            }
          })
          .catch(err => console.error(`Failed to find document: ${err}`))
      }
    })
    .catch(err => console.error(`Failed to find document: ${err}`));
  else {
    await Message.find({})
      .then(messages => {
        if(messages.length) {        
          res.send(JSON.stringify(messages))
        } else {
          res.send(JSON.stringify(false))
        }
      })
      .catch(err => console.error(`Failed to find documents: ${err}`))
  }
})

// Find user Message
router.get('/user/', async (req,res) => {
  const {chat_id} = req.query

  if(chat_id) await Message.findOne({"user.chat_id":chat_id})
    .then(async message => {
      res.setHeader('content-Type', 'application/json')
      if(message) {        
        res.send(JSON.stringify(message))
      } else {
        res.send(JSON.stringify(false))
      }
    })
    .catch(err => console.error(`Failed to find document: ${err}`));
  
  
})

// Find driver Messages
router.get('/driver/', async (req,res) => {
  const {chat_id} = req.query

  if(chat_id) await Message.find({"drivers":{ $elemMatch: {chat_id}}})
    .then(async messages => {
      res.setHeader('content-Type', 'application/json')           
      if(messages.length) {
        res.send(JSON.stringify(messages))
      } else {
        res.send(JSON.stringify(false))
      }
    })
    .catch(err => console.error(`Failed to find document: ${err}`))  
})

router.delete('/', async (req,res) => {
  const {chat_id} = req.query

  res.setHeader('content-Type', 'application/json')
  if(chat_id) await Message.findOneAndDelete({"user.chat_id":chat_id})
    .then(message => {
      if(message) res.send(JSON.stringify(`Successfully remove ${chat_id} user message`));
      else res.send(`Can't remove ${chat_id} user message`);
    })
    .catch(err => console.error(`Failed to find message: ${err.message}`));
})

module.exports = router