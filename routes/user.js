const router = require('express').Router()
const User = require('../models/User')


// Create User
router.post('/create', async (req, res) => {
  const user = new User(req.body)

  

  res.setHeader('Content-Type','application/json')
  try {
    const savedUser = await user.save()
    
    res.send(JSON.stringify(`${req.body.chat_id} user has been created`))
  } catch (e) {
    res.status(400).send(JSON.stringify(`Failed to create document: ${e.message}`))
  }
})

// Edit User
router.post('/edit', async (req,res) => {
  await User.findOneAndUpdate({ chat_id:req.body.chat_id }, req.body, {
    useFindAndModify: false,
    new: true
  }, (err, doc) => {
    res.setHeader('content-Type', 'application/json')
    if(err) res.send(JSON.stringify(`Fail to edit ${req.body.chat_id} user:\n${err.message}`));
    else{
      if(doc) res.send(JSON.stringify(`${req.body.chat_id} user has been edited`));
      else res.send(JSON.stringify("No users match"));
    }    
  })
})

// Find User
router.get('/', async (req,res) => {
  const {chat_id} = req.query

  res.setHeader('content-Type', 'application/json')
  if(chat_id) await User.findOne({chat_id})
    .then(user => {
      if(user) res.send(JSON.stringify(user));
      else res.send(JSON.stringify(false));
    })
    .catch(err => console.error(`Failed to find document: ${err.message}`));
  else if(!Object.keys(req.query).length) await User.find({})
    .then(users => {
      if(users.length) res.send(JSON.stringify(users));
      else  res.send(JSON.stringify(false));
    })
    .catch(err => console.error(`Failed to find document: ${err.message}`));
})


module.exports = router