const router = require('express').Router()
const Request = require('../models/Request')
const Address = require('../models/Address')

// Create Request
router.post('/create', async (req, res) => {
  const request = new Request(req.body)

  res.setHeader('Content-Type','application/json')
  try {
    const savedRequest = await request.save()
      
    res.send(`${req.body.chat_id} user request has been created`)
  } catch (e) {
    res.status(400).send(JSON.stringify(`Failed to create ${req.body.chat_id} user request: ${e.message}`))
  }
})

// Create Address
router.post('/createaddress', async (req, res) => {
  const {lat, lon} = req.body

  await Address.findOne({lat, lon})
    .then(async address => {
      res.setHeader('content-Type', 'application/json')      
      if(address) res.send(JSON.stringify('Address is already exists'));
      else {
        const newAddress = new Address(req.body)

        try {
          const savedAddress = await newAddress.save()

          res.send(JSON.stringify(`Address has been added`))
        } catch (e) {
          res.status(400).send(JSON.stringify(`Failed to create ${req.body.chat_id} address: ${e.message}`))
        }
      }
    })
    .catch(err => console.error(`Failed to find address: ${err.message}`)) 
})

// Find Address
router.get('/findaddress', async (req, res) => {
  const {lat, lon} = req.query

  await Address.find({})
    .then(async addresses => {
      res.setHeader('content-Type', 'application/json')      
      if(addresses.length){
        const result = addresses.find(({boundingbox}) => {
          let isLat = false
          let isLon = false

          if(boundingbox[0] < boundingbox[1]) isLat = boundingbox[0] <= lat && lat <= boundingbox[1]
          if(boundingbox[1] < boundingbox[0]) isLat = boundingbox[1] <= lat && lat <= boundingbox[0]
          if(boundingbox[2] < boundingbox[3]) isLon = boundingbox[2] <= lon && lon <= boundingbox[3]
          if(boundingbox[3] < boundingbox[2]) isLon = boundingbox[3] <= lon && lon <= boundingbox[2]

          return isLat && isLon
        })

        if(result) {
          const request_address = {}
          for(let lang in result.address) {
            const { house_number, road, town } = result.address[lang]
            if (road) request_address[lang] = { 
              town,
              street: road,
              building: house_number ? house_number : '',
              comment:'',
              lat: lat,
              lng: lon
            }
          }

          res.send(JSON.stringify(request_address))
        }
        else res.send(JSON.stringify(false))
      }
      else res.send(JSON.stringify(false))
    })
    .catch(err => console.error(`Failed to find address: ${err.message}`)) 
})

// Edit Request
router.post('/edit', async (req,res) => {  
  await Request.findOneAndUpdate({chat_id:req.body.chat_id, state: 'processed'}, req.body, {
    useFindAndModify: false,
    new: true
  }, (err, doc) => {
    res.setHeader('content-Type', 'application/json')
    if(err) res.send(JSON.stringify(`Fail to edit ${req.body.chat_id} user request:\n${err.message}`));
    else{
      if(doc) res.send(JSON.stringify(`${req.body.chat_id} user request has been edited`));
      else res.send(JSON.stringify("No requests match"));
    } 
  })  
})

// Current Request
router.get('/current/', async (req,res) => {
  const {chat_id} = req.query
  await Request.findOne({chat_id, state: "processed"})
    .then(request => {      
      res.setHeader('content-Type', 'application/json')
      if(request) res.send(JSON.stringify(request));
      else res.send(JSON.stringify(false));
    })
    .catch(err => console.error(`Failed to find ${chat_id} user request: ${err.message}`))
})

// Find Request
router.get('/', async (req,res) => {
  const { chat_id, order_id } = req.query

  res.setHeader('content-Type', 'application/json')
  if(chat_id) await Request.find({chat_id}).sort( { complete_time: -1 } )
    .then(async requests => {
      if(requests.length) res.send(JSON.stringify(requests));
      else res.send(JSON.stringify(false));
    })
    .catch(err => console.error(`Failed to find request: ${err.message}`));
  if(order_id) await Request.findOne({order_id})
    .then(request => {
      if(request) res.send(JSON.stringify(request));
      else res.send(JSON.stringify(false))
    })
    .catch(err => console.error(`Failed to find request: ${err.message}`));
  else if(!Object.keys(req.query).length) await Request.find({})
    .then(requests => {
      if(requests) res.send(JSON.stringify(requests));
      else res.send(JSON.stringify(false));
    })
    .catch(err => console.error(`Failed to find request: ${err.message}`));
})

// Find driver Request
router.get('/driver/', async (req,res) => {
  const {chat_id} = req.query

  res.setHeader('content-Type', 'application/json')
  if(chat_id) await Request.find({"drivers": { $elemMatch: {chat_id}}})
    .then(requests => {
      if(requests.length) res.send(JSON.stringify(requests));
      else res.send(JSON.stringify(false));
    })
    .catch(err => console.error(`Failed to find request: ${err.message}`));
  else  res.send(JSON.stringify(false));
})

router.delete('/', async (req,res) => {
  const {chat_id} = req.query

  res.setHeader('content-Type', 'application/json')
  if(chat_id) await Request.findOneAndDelete({chat_id, state: "processed"})
    .then(request => {
      if(request) res.send(JSON.stringify(`Successfully remove ${chat_id} user request`));
      else res.send(`Can't remove ${chat_id} user request`);
    })
    .catch(err => console.error(`Failed to find request: ${err.message}`));
})


module.exports = router