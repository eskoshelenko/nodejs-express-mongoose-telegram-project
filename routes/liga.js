const router = require('express').Router()
const {Liga, Request, Message, User, Driver, Settings} = require('../lib/manipulation')
const {Messages} = require('../lib/messages')
const { path } = require('../config')
const { Log } = require('../lib/log')
const log = new Log(path + '/log/liga')
const { locations, companies } = require('../liga/locality')
const { userBot, driverBot } = require('../bot')
const asterisk = require('../lib/asteriskAutoInfo')

const state = {
  create: '1',
  set: '2',
  arrive: '3',
  onRoad: '4',
  done: '5',
  cansel: '6',
  timeout: '7',
  reserve: '8',
  label(value) {
    return Object.entries(this).find(([_, val]) => val === value)[0].toUpperCase()
  }
}

const users = new User(),  drivers = new Driver(), requests = new Request(), messages = new Message(), liga = new Liga(companies, locations), settings = new Settings()
const logo = `\u24C1\u24BE\u24BC\u24B6\n\u24C9\u24B6\u24E7\u24D8\n`.toUpperCase()
const sender = new Messages('ru', logo)
const timeoutObj = {}
const minute = 60 * 1000, second = 1000
const logoOffset = 50

router.get('/request/state_changes/', async (req, res) => {
  const { req_state, order_id, driver_id, client_phone, company_id } = req.query

  switch(req_state) {
    case state.create:
      break
    case state.set:            
      break
    case state.arrive:
      requests.findByOrderId(order_id)
        .then(request => {
          if(request)  users.find(request.chat_id)
            .then(({ chat_id, menu_message_id, language_code, shortMode }) => drivers.ligaFind(driver_id)
              .then(driver => {
                sender.clearHistory(userBot, chat_id, [menu_message_id])
                sender.arrived([userBot, chat_id], [request, driver, language_code, shortMode, logoOffset])    
                  .then(res => {
                    messages.edit({user: res, drivers: []})
                  })
            }))
          else return
        })   
      break
    case state.onRoad:
      break
    case state.done:           
      break 
    case state.cansel:
      break
    case state.timeout:
      break
    case state.reserve:
      break
  }
//  log.save(`Request: { ID: ${order_id}, Company: ${company_id}, State: ${state.label(req_state)}, Driver: ${driver_id}, userPhone: ${client_phone} }`)

  driver_id != "None" && drivers.ligaFind(driver_id).then(driver => console.log(driver))
  liga.driversLocs(company_id).then(locs => log.save(JSON.stringify({
    ...locs, 
    order_id, company_id, req_state, driver_id, client_phone, state: state.label(req_state)
  }, null, 0)))
})

router.get('/request/done/', async (req, res) => {
  const { rateDelay } = await settings.find()
  const {  order_id, driver_id, price } = req.query

  order_id && requests.findByOrderId(order_id)
  .then(request => {
    if(request)  users.find(request.chat_id)
      .then(user => {
        sender.clearHistory(userBot, user.chat_id, [user.menu_message_id])
        sender.rate([userBot, user.chat_id],
          [request, user.language_code, user.shortMode, logoOffset])
          .then(res => {
            messages.edit({user: res, drivers: []})
            requests.edit({...request, state: 'completed'})

            timeoutObj[res.chat_id] = setTimeout(() => sender.userMenu([userBot, res.chat_id, res.message_id], 
              [user, user.language_code, user.shortMode, logoOffset])
              .then( ({message_id}) => {
                users.edit({...user, menu_message_id: message_id})
                messages.delete(res.chat_id)
              }), rateDelay * minute)
          })
      })
    else return
  })
  
 log.save(`Request: { ID: ${order_id}, Driver: ${driver_id}, state:"DONE", price: ${price} }`)
})


router.get('/request/driver_get_request/', async (req, res) => {
  const { order_id, driver_id, company_id } = req.query

  order_id && requests.findByOrderId(order_id)
    .then(request => {
      if(request)  {       
        users.find(request.chat_id)
          .then(({ chat_id, menu_message_id, language_code, shortMode }) => {
            messages.findUserMsg(chat_id)
              .then(message=> {
                message.drivers.forEach(async ({chat_id, history}) => {
                const currentDriver = await drivers.find(chat_id)
                  
                sender.clearHistory(driverBot, chat_id, history)
                sender.getted([driverBot, chat_id], [ currentDriver.language_code, currentDriver.shortMode, logoOffset])
                  .then(({chat_id, message_id}) =>  timeoutObj[chat_id] = setTimeout(() => driverBot.deleteMessage(chat_id, message_id), 10 * 1000))
                })
              })
            drivers.ligaFind(driver_id).then(driver => {
              sender.yourCar([userBot, chat_id, menu_message_id], 
                [ request, driver, language_code, shortMode, logoOffset ])    
                .then(res => {
                  messages.edit({ user: res, drivers: [] })
                  requests.edit({...request, drivers: [driver]})
                })
            })
          })
      }
      else return
    }) 

  log.save(`Request: { ID: ${order_id}, Company: ${company_id}, State: TAKE, Driver: ${driver_id} }`)
})

router.get('/driver/state_changes/', async (req, res) => {
  const {driver_id, state, diver_call_id, company_id} = req.query

 log.save(`Driver: { ID: ${driver_id}, Company: ${company_id}, State: ${state} }`)
})

module.exports = router