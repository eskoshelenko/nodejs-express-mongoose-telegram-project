const router = require('express').Router()
const Driver = require('../models/Driver')
const LigaDriver = require('../models/LigaDriver')

// Create Driver
router.post('/create', async (req, res) => {

  await Driver.findOneAndUpdate({ phone:req.body.phone }, req.body, {
    useFindAndModify: false,
    new: true
  }, async (err, doc) => {
    res.setHeader('content-Type', 'application/json')
    if(err) res.send(JSON.stringify(err.message));
    else{
      if(doc) res.send(JSON.stringify(`${req.body.chat_id} driver has been edited`));
      else {
        const driver = new Driver(req.body)
  
        try {
          const savedDriver = await driver.save()

          res.send(JSON.stringify(`${req.body.chat_id} driver has been created`))
        } catch (e) {
          res.status(400).send(`Failed to create document: ${e.message}`);
        }
      }
    }
  })   
})

// Edit Driver
router.post('/edit', async (req,res) => {
  await Driver.findOneAndUpdate({ chat_id:req.body.chat_id }, req.body, {
    useFindAndModify: false,
    new: true
  }, (err, doc) => {
    res.setHeader('content-Type', 'application/json')
    if(err) res.send(JSON.stringify(err.message));
    else{
      if(doc) res.send(JSON.stringify(`${req.body.chat_id} driver has been edited`));
      else res.send(JSON.stringify("No drivers match"));
    }    
  })  
})

// Find Driver
router.get('/', async (req,res) => {
  const {chat_id} = req.query

  res.setHeader('content-Type', 'application/json')
  if(chat_id) await Driver.findOne({chat_id})
    .then(driver => {
      if(driver) res.send(JSON.stringify(driver));
      else res.send(JSON.stringify(false));
    })
    .catch(err => console.error(`Failed to find document: ${err.message}`));
  else if(!Object.keys(req.query).length) await Driver.find({})
    .then(drivers => {
      if(drivers.length) res.send(JSON.stringify(drivers));
      else res.send(JSON.stringify(false));
    })
    .catch(err => console.error(`Failed to find document: ${err.message}`));
})

// Create Liga Driver
router.post('/liga/create', async (req, res) => {

  await LigaDriver.findOneAndUpdate({ driver_id: req.body.driver_id }, req.body, {
    useFindAndModify: false,
    new: true
  }, async (err, doc) => {
    res.setHeader('content-Type', 'application/json')
    if(err) res.send(JSON.stringify(err.message));
    else{
      if(doc) res.send(JSON.stringify(`${req.body.driver_id} driver has been edited`));
      else {
        const driver = new LigaDriver(req.body)
  
        try {
          const savedDriver = await driver.save()

          res.send(JSON.stringify(`${req.body.driver_id} driver has been created`))
        } catch (e) {
          res.status(400).send(`Failed to create document: ${e.message}`);
        }
      }
    }
  })   
})

// Find Driver in Liga
router.get('/liga/', async (req,res) => {
  const { driver_id } = req.query

  res.setHeader('content-Type', 'application/json')
   if(driver_id) await LigaDriver.findOne({driver_id})
    .then(driver => {
      if(driver) {
        driver.name = driver.name.split(" ")[1] || driver.name
        res.send(JSON.stringify(driver));
      }
      else res.send(JSON.stringify(false));
    })
    .catch(err => console.error(`Failed to find document: ${err.message}`));
  else if(!Object.keys(req.query).length) await LigaDriver.find({})
    .then(drivers => {
      if(drivers.length) res.send(JSON.stringify(drivers));
      else res.send(JSON.stringify(false));
    })
    .catch(err => console.error(`Failed to find document: ${err.message}`));
})


module.exports = router