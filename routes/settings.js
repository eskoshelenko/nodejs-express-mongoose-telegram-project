const router = require('express').Router()
const Settings = require('../models/Settings')
const condition = 'keyForBot'

// Create Settings
router.post('/create', async (req, res) => {
  res.setHeader('Content-Type','application/json')
  await Settings.findOneAndUpdate({ condition }, req.body, {
    useFindAndModify: false,
    new: true
  }, async (err, doc) => {
    if(err) res.send(JSON.stringify(`Fail to edit settings:\n${err.message}`));
    else{
      if(doc) res.send(JSON.stringify(`Settings has been edited`));
      else {
        const settings = new Settings({...req.body, condition})
  
        try {
          const savedSettings = await settings.save()

          res.send(JSON.stringify(req.body))
        } catch (e) {
          res.status(400).send(JSON.stringify(`Failed to create document: ${e.message}`))
        }
      }
    }    
  }) 
})

// Edit Settings
router.post('/edit', async (req,res) => {
  await Settings.findOneAndUpdate({ condition }, req.body, {
    useFindAndModify: false,
    new: true
  }, (err, doc) => {
    res.setHeader('content-Type', 'application/json')
    if(err) res.send(JSON.stringify(`Fail to edit settings:\n${err.message}`));
    else{
      if(doc) res.send(JSON.stringify(`Settings has been edited`));
      else res.send(JSON.stringify("No settings match"));
    }    
  })
})

// Find Settings
router.get('/', async (req,res) => {
  res.setHeader('Content-Type','application/json')
  await Settings.find({})
    .then(async settings => {
      if(settings.length) res.send(JSON.stringify(settings[0]));
      else res.send(JSON.stringify("No settings match"));
    })
    .catch(err => console.error(`Failed to find document: ${err.message}`))
})


module.exports = router