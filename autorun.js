const https = require('http')
const { exec } = require("child_process")
const filePath = '/home/ujin/ezTaxi'
const hostname = '127.0.0.1'
const path = '/isup'
const headers = {'User-Agent': 'Mozilla/5.0'}
const second = 1000
const delay = 30 * second

const watchObj = {
  liga: {options: {hostname, path, port: 3001, headers}, fileName: 'ligaBot.js'},
//   bot: {options: {hostname, path, port: 3002, headers}, fileName: 'bot.js'},
  server: { options: {hostname, path, port: 3000, headers}, fileName: 'app.js'}
} 

for(let prop in watchObj) {
  setInterval(() => https.get(watchObj[prop].options, res => res.on('data', chunk => console.log(`${prop} is up: ${chunk}`)))
    .on("error", e => exec(`node ${filePath}/${watchObj[prop].fileName}`, (error, stdout, stderr) => {
      if (e) console.log(`error: ${e.message}`)
      if (stderr) console.log(`stderr: ${stderr}`)
      console.log(`${stdout}`)
    })), delay)
}