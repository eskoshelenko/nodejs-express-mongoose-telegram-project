const {Schema, model} = require('mongoose')

const addressSchema = Schema({
  boundingbox:[String],
  lat:Number,
  lon:Number,
  display_name:{
    ru: String,
    ua: String,
    en: String
  },
  type:String,
  importance:Number,
  address:{
    ru: {
      house_number:String,
      road:String,
      town:String,
      county:String,
      state:String,
      postcode:Number,
      country:String,
      country_code:String
    },
    ua: {
      house_number:String,
      road:String,
      town:String,
      county:String,
      state:String,
      postcode:Number,
      country:String,
      country_code:String
    },
    en: {
      house_number:String,
      road:String,
      town:String,
      county:String,
      state:String,
      postcode:Number,
      country:String,
      country_code:String
    }
  },
  geojson:{}
})

module.exports = model("Address", addressSchema)