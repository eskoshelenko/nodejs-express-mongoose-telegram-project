const {Schema, model} = require('mongoose')

const messageSchema = Schema({
  user: {
    chat_id: Number,
    message_id: Number,
    message: {
      text:String,
      reply_markup:{}
    },
    history: [Number]
  },
  drivers: [
    {
      chat_id:Number,
      message_id: Number,
      message: {},
      history: [Number]
    }
  ]
},
{
  toObject: {
    transform: function (doc, ret) {
      delete ret._id;
    }
  },
  toJSON: {
    transform: function (doc, ret) {
      delete ret._id;
    }
  }
})

module.exports = model("Message", messageSchema)