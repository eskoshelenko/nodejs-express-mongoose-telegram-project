const {Schema, model} = require('mongoose')

const driverSchema = Schema({
  chat_id: Number,
  driver_id: Number,
  menu_message_id: Number,
  name: {
    type: String,
    required: true
  },
  phone: {
    type: String,
    required: true,
    unique: true
  },
  balance: Number,
  completed:[Number],
  edit: [String],
  car_model: String,
  car_color: String,
  car_number: String,
  rate: {type: Number, default: 3},
  onLunch: {type: Boolean, default: false},
  ban: {type: Boolean, default: false},
  language_code: String,
  shortMode: {type: Boolean, default: false}
})

module.exports = model("Driver", driverSchema)