const {Schema, model} = require('mongoose')

const requestSchema = Schema({
  chat_id: Number,
  name: String,
  phone: String,
  state: String,
  order_id: Number,
  drivers:[{
    chat_id: Number,
    message_id: String,
    name: String,
    phone: String,
    car_model: String,
    car_color: String,
    car_number: String,
    rate: Number,
    language_code: String,
    shortMode: Boolean
  }],
  pickup_address:{
    ru: {
      town:{
        type: String,
        required: true
      },
      street:{
        type: String,
        required: true
      },
      building:{
        type: String
      },
      comment:String,
      lat:{
        type: Number,
        required: true
      },
      lng:{
        type: Number,
        required: true
      }
    },
    ua: {
      town:{
        type: String,
        required: true
      },
      street:{
        type: String,
        required: true
      },
      building:{
        type: String
      },
      comment:String,
      lat:{
        type: Number,
        required: true
      },
      lng:{
        type: Number,
        required: true
      }
    },
    en: {
      town:{
        type: String,
        required: true
      },
      street:{
        type: String,
        required: true
      },
      building:{
        type: String
      },
      comment:String,
      lat:{
        type: Number,
        required: true
      },
      lng:{
        type: Number,
        required: true
      }
    }    
  },
  pickup_time:String,
  create_time:String,
  complete_time:String,
  car_type:String,
  car_extras:[
    {
      type: String
    }
  ],
  driver_extras:[
    {
      type: String
    }
  ],
  comment:String,
  destinations:[
     {
       ru: {
        town: String,
        street: String,
        building: String,
        comment: String,
        lat: Number,
        lng: Number
       },
       ua: {
        town: String,
        street: String,
        building: String,
        comment: String,
        lat: Number,
        lng: Number
       },
       en: {
        town: String,
        street: String,
        building: String,
        comment: String,
        lat: Number,
        lng: Number
       }
     }
  ]
})

module.exports = model("Request", requestSchema)