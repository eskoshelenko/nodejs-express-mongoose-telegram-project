const {Schema, model} = require('mongoose')

const settingsSchema = Schema({
  condition: {type:String, unique: true},
  maxRows: Number,
  alertDelay: Number,
  rateDelay: Number,
  deliveryDelay: Number
})

module.exports = model("Settings", settingsSchema)