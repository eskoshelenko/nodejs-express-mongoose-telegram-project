const {Schema, model} = require('mongoose')

const userSchema = Schema({
  chat_id: {
    type: Number,
    required: true,
    unique: true
  },
  menu_message_id: Number,
  name: {
    type: String,
    required: true
  },
  phone: {
    type: String,
    required: true,
    unique: true
  },
  canselCount: {
    type: Number,
    default: 0
  },
  completeCount: {
    type: Number,
    default: 0
  },
  bonus: {
    type: Number,
    default: 0
  },
  type: {type: String, default: 'user'},
  ban: {type: Boolean, default: false},
  shortMode: {type: Boolean, default: false},
  language_code: String,
  edit: [String],
  uuid: String
})

module.exports = model("User", userSchema)