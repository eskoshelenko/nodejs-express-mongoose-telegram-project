const {Schema, model} = require('mongoose')

const driverSchema = Schema({
  driver_id: Number,
  call_id: Number,
  name: String,
  phone: {
    type: String,
    unique: true
  },
  car_model: String,
  car_color: String,
  car_number: String
})

module.exports = model("LigaDriver", driverSchema)